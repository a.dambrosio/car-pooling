package it.uniba.di.sms.max2d.carpooling;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class User {

    public static final int MOBILITY_DEMO = -1;
    public static final int USER_DEMO = -2;
    public static final String MOBILITY_DEMO_EMAIL = "mobility@manager.com";
    public static final String USER_DEMO_EMAIL = "ospite@ospite.it";
    public static final String DEMO_PASSWORD = "123456";

    public int id;
    public String firebaseUid;
    public String email;
    public String name;
    public String surname;
    public int companyId;
    public int role;
    public String data;
    public String birthday;
    public String address;
    public String avatarUrl;
    public String phone;
    public int companyVerified;

    public static final int COMPANY_VERIFIED = 1;
    public static final int COMPANY_NOT_VERIFIED = 0;
    public static final int SIMPLE_USER = 0;
    public static final int MOBILITY_MANAGER = 1;


    public String auto;


    public User (int id, String firebaseUid, String email, String name, String surname, int companyId, int role,
                 String birthday, String address, String avatarUrl, String phone, int companyVerified ) {
        this.id = id;
        this.firebaseUid = firebaseUid;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.companyId = companyId;
        this.role = role;
        this.birthday = birthday;
        this.address = address;
        this.avatarUrl = avatarUrl;
        this.phone = phone;
        this.companyVerified = companyVerified;
    }

    public User (JSONObject user) {
        try {
            this.id = user.getInt("idUser");
            this.email = user.getString("emailUser");
            this.name = user.getString("nameUser");
            this.surname = user.getString("surnameUser");
            this.companyId = user.getInt("FKCompanyUser");
            this.role = user.getInt("roleUser");
            this.birthday = user.getString("birthdayUser");
            this.address = user.getString("addressUser");
            this.avatarUrl = user.getString("avatarUser");
            this.phone = user.getString("phoneUser");
            this.companyVerified = user.getInt("companyVerifiedUser");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Restituisce un oggetto in formato JSON con i dati dell'utente
     */
    public JSONObject toJsonObject() {
        try {
            JSONObject user = new JSONObject();
            user.put("firebaseUIDUser", this.firebaseUid);
            user.put("emailUser", this.email);
            user.put("nameUser", this.name);
            user.put("surnameUser", this.surname);
            user.put("FKCompanyUser", this.companyId);
            user.put("roleUser", this.role);
            user.put("birthdayUser", this.birthday);
            user.put("addressUser", this.address);
            user.put("avatarUser", this.avatarUrl);
            user.put("phoneUser", this.phone);
            return user;
        } catch (Exception e) {
            return null;
        }
    }
}
