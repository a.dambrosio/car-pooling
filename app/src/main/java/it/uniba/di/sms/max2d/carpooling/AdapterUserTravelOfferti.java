package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AdapterUserTravelOfferti extends RecyclerView.Adapter<AdapterUserTravelOfferti.UserViewHolder> {
    private User[] users;
    Context mContext;
    LayoutInflater inflater;
    TravelRequest[] mTravelRequest;

    public AdapterUserTravelOfferti(User[] users, TravelRequest[] travelRequest, Context mContext) {
        this.mTravelRequest = travelRequest;
        this.users = users;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV;
        TextView notVerifiedTV;
        ImageView avatarImage;
        CardView cv;
        TextView cellular;


        public UserViewHolder(View view) {
            super(view);
            nameTV = (TextView) view.findViewById(R.id.ProfileNameTravel);
            cv = (CardView) view.findViewById(R.id.card_viewUserTravel);
            avatarImage = (ImageView) view.findViewById(R.id.ProfileAvatarImageTravel);
            notVerifiedTV = (TextView) view.findViewById(R.id.notVerifiedTVTravel);
            cellular = (TextView) view.findViewById(R.id.cellularOfferti);

        }


    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_travel_offerti, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {
        try {
            final User user = users[position];
            final TravelRequest t = mTravelRequest[position];
            Picasso avatarLoader = Picasso.get();
            final String name = user.name + " " + user.surname;

            holder.nameTV.setText(name);

            holder.nameTV.setTextColor(mContext.getResources().getColor(android.R.color.black));
            holder.notVerifiedTV.setTextColor(mContext.getResources().getColor(android.R.color.black));
            holder.notVerifiedTV.setVisibility(View.VISIBLE);
            holder.cellular.setText(user.phone);
            holder.cellular.setTextColor(mContext.getResources().getColor(android.R.color.black));

            switch (t.status) {
                case TravelRequest.ACCEPTED_REQUEST:
                    holder.notVerifiedTV.setText(getApplicationContext().getResources().getString(R.string.accepted));
                    break;
                case TravelRequest.REJECTED_REQUEST:
                    holder.notVerifiedTV.setText(getApplicationContext().getResources().getString(R.string.rejected));
                    break;
                case TravelRequest.PENDING_REQUEST:
                    holder.notVerifiedTV.setText(getApplicationContext().getResources().getString(R.string.pending_requests));
                    break;
            }


            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //
                }
            });

            holder.cv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (holder.notVerifiedTV.getText().toString().equals(getApplicationContext().getResources().getString(R.string.pending_requests))) {
                        CharSequence options[] = new CharSequence[]{getApplicationContext().getResources().getString(R.string.accept), getApplicationContext().getResources().getString(R.string.reject)};
                        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);


                        builder.setTitle(getApplicationContext().getResources().getString(R.string.select_choise));
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Map<String, Object> Map = new HashMap<>();
                                if (which == 0) {
                                    Map.put("newStatus", TravelRequest.ACCEPTED_REQUEST);
                                } else if (which == 1) {
                                    Map.put("newStatus", TravelRequest.REJECTED_REQUEST);
                                }
                                ApiUtil.updateTravelRequest(t.id, Map);
                                reload();

                            }
                        });

                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                        return true;
                    }
                    return false;
                }
            });


            if (user.avatarUrl.isEmpty()) {
                avatarLoader.load(R.drawable.default_image).into(holder.avatarImage);
            } else {
                //avatarLoader.load(avatarUrl).into(holder.avatarImage);
                avatarLoader.load(user.avatarUrl)
                        .into(holder.avatarImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap imageBitmap = ((BitmapDrawable) holder.avatarImage.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                                holder.avatarImage.setImageDrawable(imageDrawable);
                            }

                            @Override
                            public void onError(Exception $e) {
                                holder.avatarImage.setImageResource(R.drawable.default_image);
                            }
                        });
            }


        } catch (Exception e) {
            reload();
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return users.length;
    }

    private void reload(){
        ((Activity)mContext).finish();
        ((Activity)mContext).overridePendingTransition(0, 0);
        ((Activity)mContext).startActivity(((Activity) mContext).getIntent());
        ((Activity)mContext).overridePendingTransition(0, 0);

    }


}
