package it.uniba.di.sms.max2d.carpooling;

import org.json.JSONObject;

public class WorkingHour {

    public int id;
    public String start;
    public String end;
    public int company;

    public WorkingHour(int id, String start, String end, int company) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.company = company;
    }

    public WorkingHour(JSONObject workingHour) {
        try {
            this.id = workingHour.getInt("idWH");
            this.start = workingHour.getString("startWH");
            this.end = workingHour.getString("endWH");
            this.company = workingHour.getInt("FKCompanyWH");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJsonObject() {
        try {
            JSONObject wh = new JSONObject();
            wh.put("startWH", this.start);
            wh.put("endWH", this.end);
            wh.put("FKCompanyWH", this.company);
            return wh;
        } catch (Exception e) {
            return null;
        }
    }



}
