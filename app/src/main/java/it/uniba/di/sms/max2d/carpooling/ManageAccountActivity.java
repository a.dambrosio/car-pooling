package it.uniba.di.sms.max2d.carpooling;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by danie on 12/06/2018.
 */

public class ManageAccountActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    Toolbar mtoolbar;

    EditText userName;
    EditText email;
    EditText telephoneNew;
    EditText birthDay;
    EditText addressNew;
    EditText company;

    TextInputLayout textInputLayoutTelePhone;
    TextInputLayout textInputLayoutAddress;

    Button buttonEdit;

    String MESSAGE_ERROR_EDIT;
    String EDIT_ADDED_CORRECT;
    String EDIT_ADDED_WRONG;

    ImageView userImage;

    Switch switchEdit;


    private FirebaseUser FireBaseUser;
    private User user;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_account);

        MESSAGE_ERROR_EDIT = getResources().getString(R.string.messagge_error_field);
        EDIT_ADDED_CORRECT = getResources().getString(R.string.message_edit_correct);
        EDIT_ADDED_WRONG = getResources().getString(R.string.message_edit_wrong);


        FireBaseUser = FirebaseAuth.getInstance().getCurrentUser();
        user = ApiUtil.getUser(FireBaseUser.getUid());


        if (getSupportActionBar() != null)
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);


        userName = (EditText) findViewById(R.id.editTextManageName);
        email = (EditText) findViewById(R.id.editTextManageEmail);
        telephoneNew = (EditText) findViewById(R.id.editTextManageTelephone);
        birthDay = (EditText) findViewById(R.id.editTextManageBirth);
        addressNew = (EditText) findViewById(R.id.editTextManageAddress);
        company = (EditText) findViewById(R.id.editTextManageCompany);
        switchEdit = (Switch) findViewById(R.id.switchEdit);
        userImage = (ImageView) findViewById(R.id.UserImage);

        textInputLayoutTelePhone = (TextInputLayout) findViewById(R.id.inputLayoutTelephoneManage);
        textInputLayoutAddress = (TextInputLayout) findViewById(R.id.inputLayoutAddressManage);

        buttonEdit = (Button) findViewById(R.id.buttonEdit);
        buttonEdit.setOnClickListener(edit);

        int display = getResources().getConfiguration().orientation;

        if (display == Configuration.ORIENTATION_LANDSCAPE) {

            Log.d("value", "value" + switchEdit.isChecked());


            if (switchEdit.isChecked()) {
                telephoneNew.setEnabled(true);
                addressNew.setEnabled(true);
                buttonEdit.setEnabled(true);
            } else {
                telephoneNew.setEnabled(false);
                addressNew.setEnabled(false);
                buttonEdit.setEnabled(false);

            }
        }

        initializeVisibility(false);

        manageEdit();

        loadDataFromDatabase();

        validate();

        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbarManageAccount);


        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = user.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(user.name + " " + user.surname);

        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("switchCheked", switchEdit.isChecked());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        boolean isSwitchChecked = savedInstanceState.getBoolean("switchCheked");

        if (isSwitchChecked) {
            switchEdit.setChecked(true);
            telephoneNew.setEnabled(true);
            addressNew.setVisibility(View.GONE);
        }


    }

    public View.OnClickListener edit = new View.OnClickListener() {

        String address;
        String telephone;

        @Override
        public void onClick(View v) {

            boolean valid = validate();

            if (valid) {
                address = getAddress();
                telephone = getTelephone();

                Map<String, Object> newData = new HashMap<>();
                newData.put("address", address);
                newData.put("phone", telephone);
                Snackbar snackbar;

                if (ApiUtil.updateUser(user.id, newData)) {
                    snackbar = Snackbar.make(findViewById(R.id.ManageLayout), EDIT_ADDED_CORRECT, Snackbar.LENGTH_LONG);
                    telephoneNew.setEnabled(false);
                    addressNew.setEnabled(false);
                    switchEdit.setChecked(false);

                /*
                Visualizza una snackBar come feedback per il corretto inseriemnto del passaggio
                 */

                } else {
                    snackbar = Snackbar.make(findViewById(R.id.main_content), EDIT_ADDED_WRONG, Snackbar.LENGTH_LONG);
                }

                snackbar.show();


            }
        }
    };


    private void initializeVisibility(boolean enabled) {

        userName.setEnabled(enabled);
        email.setEnabled(enabled);
        telephoneNew.setEnabled(enabled);
        birthDay.setEnabled(enabled);
        company.setEnabled(enabled);
        addressNew.setEnabled(enabled);
        buttonEdit.setVisibility(View.GONE);

    }


    private void manageEdit() {
        switchEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchEdit.isChecked()) {
                    addressNew.setEnabled(true);
                    telephoneNew.setEnabled(true);
                    buttonEdit.setVisibility(View.VISIBLE);
                } else {
                    addressNew.setEnabled(false);
                    telephoneNew.setEnabled(false);
                    buttonEdit.setVisibility(View.GONE);
                }
            }

        });
    }


    private void loadDataFromDatabase() {

        userName.setText(user.name + " " + user.surname);
        email.setText(user.email);
        telephoneNew.setText(user.phone);
        birthDay.setText(user.birthday);
        addressNew.setText(user.address);

        Company c = ApiUtil.getCompany(user.companyId);
        company.setText(c.name);
/*


        String avatarUrl = user.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(userImage);
        } else {
            //avatarLoader.load(avatarUrl).into(holder.avatarImage);
            avatarLoader.load(avatarUrl)
                    .into(userImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) userImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(ManageAccountActivity.this.getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            userImage.setImageDrawable(imageDrawable);
                        }
                        @Override
                        public void onError(Exception $e) {
                            userImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }
        */


    }

    private boolean validate() {
        boolean valid = true;

        if (getAddress().length() == 0) {
            valid = false;
            textInputLayoutAddress.setError(MESSAGE_ERROR_EDIT);
        }

        if (getTelephone().length() == 0) {
            valid = false;
            textInputLayoutTelePhone.setError(MESSAGE_ERROR_EDIT);
        }
        return valid;
    }

    private String getTelephone() {
        return telephoneNew.getText().toString();
    }

    private String getAddress() {
        return addressNew.getText().toString();
    }


    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.manage_account_activity));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));


            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (user.id == User.MOBILITY_DEMO || user.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if (user.companyVerified != User.COMPANY_VERIFIED) {

                for (int i = 0; i < mMenu.size(); i++)
                    if (!(mMenu.getItem(i).getItemId() == R.id.Home || mMenu.getItem(i).getItemId() == R.id.Account || mMenu.getItem(i).getItemId() == R.id.LogOut))
                        mMenu.getItem(i).setVisible(false);
            }

            if (user.role == User.MOBILITY_MANAGER) {

                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));
                            finish();
                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }

                    return true;
                }
            });
        }
    });

    private Context getContext() {
        return this;
    }


}




