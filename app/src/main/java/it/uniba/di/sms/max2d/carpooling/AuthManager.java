package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

public class AuthManager {
    /**
     * Logout
     */
    public static void logout(Activity callerActivity) {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        AuthManager.redirectGuestToLogin(callerActivity);
    }

    /**
     * Reindirizzare chi non è autenticato alla pagina di login
     */
    public static void redirectGuestToLogin(Activity callerActivity) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            callerActivity.startActivity(new Intent(callerActivity, AuthActivity.class));
            callerActivity.finish();
        }
    }

    /**
     * Reindirizza chi è autenticato alla pagina principale
     */
    public static void redirectAuthenticatedToMainPage(Activity callerActivity, boolean finishActivity) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            callerActivity.startActivity(new Intent(callerActivity, MapsActivity.class));
            if (finishActivity) {
                callerActivity.finish();
            }
        }
    }
}
