package it.uniba.di.sms.max2d.carpooling;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.Html;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AdapterRichiesti extends RecyclerView.Adapter<AdapterRichiesti.PersonViewHolder> {
    private TravelRequest[] travelRequests;
    private Context mContext;
    private Map<Integer, Travel> travels;
    private Map<Integer, WorkingHour> workingHours;
    private Map<Integer, User> owners;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView data;
        TextView viaggio;
        TextView cellulare;
        TextView stato;
        TextView auto;


        TextView personAge;
        ImageView personPhoto;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);

            personPhoto = (ImageView) itemView.findViewById(R.id.user_single_image);
            data = (TextView) itemView.findViewById(R.id.date);
            viaggio = (TextView) itemView.findViewById(R.id.viagg);
            cellulare = (TextView) itemView.findViewById(R.id.cellulare);
            stato = (TextView) itemView.findViewById(R.id.stat);
            auto = (TextView) itemView.findViewById(R.id.car);
        }


    }


    AdapterRichiesti(TravelRequest[] travelRequests, Context mContext) {
        this.travelRequests = travelRequests;
        this.mContext = mContext;
        this.travels = new LinkedHashMap<>();
        this.workingHours = new LinkedHashMap<>();
        this.owners = new LinkedHashMap<>();

        for (int i=0; i<travelRequests.length; i++) {
            Map<String, Object> filters = new HashMap<>();
            filters.put("travelId", travelRequests[i].travelId);
            Travel trvl = ApiUtil.getTravels(filters)[0];
            travels.put(i, trvl);
            workingHours.put(i, ApiUtil.getWorkingHour(trvl.workingHourId));
            owners.put(i, ApiUtil.getUser(trvl.ownerId));

        }

        ((TravelActivity)mContext).hideProgressDialog();
    }

    @Override
    public int getItemCount() {
        return travelRequests.length;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.richiesti, viewGroup, false);

        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {

        try {
            final TravelRequest travelRequest = travelRequests[i];
            Map<String, Object> filters = new HashMap<>();
            filters.put("travelId", travelRequest.travelId);
            final Travel travel = travels.get(i);
            User owner = owners.get(i);
            final String phoneNumber = owner.phone;
            Picasso avatarLoader = Picasso.get();
            String avatarUrl = "http://";
            WorkingHour wh = workingHours.get(i);
            String hour = "";
            final boolean isExpiredTravel = DateUtil.isExpiredDate(DateUtil.formatToIta(travel.date));
            final boolean isFinishedToday = travelRequest.score > 0;

            if (!owner.avatarUrl.isEmpty()) {
                avatarUrl = owner.avatarUrl;
            }


            String travelType;
            final String status;

            if (travel.type == Travel.HOME_WORK) {
                travelType = getApplicationContext().getResources().getString(R.string.casalavoro);
                hour = wh.start;
            } else {
                travelType = getApplicationContext().getResources().getString(R.string.lavorocasa);
                hour = wh.end;
            }

            switch (travelRequest.status) {
                case TravelRequest.ACCEPTED_REQUEST:
                    status = getApplicationContext().getResources().getString(R.string.accepted);
                    break;
                case TravelRequest.PENDING_REQUEST:
                    status = getApplicationContext().getResources().getString(R.string.pending_requests);
                    break;
                case TravelRequest.REJECTED_REQUEST:
                    status = getApplicationContext().getResources().getString(R.string.rejected);
                    break;
                default:
                    status = "";
                    break;
            }

            personViewHolder.data.setText(Html.fromHtml( " " + DateUtil.formatToIta(travel.date) + " - " + hour));
            personViewHolder.viaggio.setText(Html.fromHtml(" " + travelType));
            personViewHolder.cellulare.setText(Html.fromHtml(" " + phoneNumber));

            personViewHolder.auto.setText(Html.fromHtml( " " + travel.car));
            personViewHolder.stato.setText(Html.fromHtml( " " + status));

            avatarLoader.load(avatarUrl)
                    .into(personViewHolder.personPhoto, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) personViewHolder.personPhoto.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            personViewHolder.personPhoto.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            personViewHolder.personPhoto.setImageResource(R.drawable.default_image);
                        }
                    });

            if (isExpiredTravel || isFinishedToday) {
                personViewHolder.cv.setCardBackgroundColor(Color.rgb(232, 232, 232));
                personViewHolder.stato.setText(Html.fromHtml("<b>" + getApplicationContext().getString(R.string.finished )+ " </b>"));
            }


            personViewHolder.cv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {

                    CharSequence options[] = null;


                    if (isExpiredTravel || isFinishedToday) {
                        options = new CharSequence[]{getApplicationContext().getString(R.string.hide)};

                    } else {
                        options = new CharSequence[]{getApplicationContext().getResources().getString(R.string.call)};
                    }

                    final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (which == 0) {
                                if (isExpiredTravel || isFinishedToday) {
                                    Map<String, Object> filters = new HashMap<>();
                                    filters.put("delete", true);
                                    ApiUtil.updateTravelRequest(travelRequest.id, filters);

                                    ((Activity) mContext).finish();
                                    ((Activity) mContext).overridePendingTransition(0, 0);
                                    ((Activity) mContext).startActivity(((Activity) mContext).getIntent());
                                    ((Activity) mContext).overridePendingTransition(0, 0);

                                } else {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse(getApplicationContext().getResources().getString(R.string.tel) + phoneNumber));
                                    if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                                        mContext.startActivity(intent);
                                    }
                                }
                            }


                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    return true;
                }
            });
            personViewHolder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isExpiredTravel && !isFinishedToday) {
                        Intent richiesti = new Intent(mContext, MapsActivity.class);
                        Bundle arg = new Bundle();
                        arg.putBoolean(MapsUtil.TRAVEL, true);
                        arg.putBoolean(MapsUtil.OFFERT, false);
                        arg.putParcelable("travel", travel);
                        arg.putString("STATUS", status);
                        richiesti.putExtras(arg);
                        mContext.startActivity(richiesti);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




}