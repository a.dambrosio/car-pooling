package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UserTravelOffertRequestActivity extends AppCompatActivity {

    public static final String EMPTY = "empty";
    public static final String OFFERT = "offert";
    public static final String REQUEST = "request";

    Toolbar mtoolbar;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    User currentUser;
    boolean isOffert = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_travel_offerti);

        Intent intent = getIntent();
        ArrayList<Travel> offert = intent.getParcelableArrayListExtra(MapsUtil.OFFERT);
        ArrayList<Travel> required = intent.getParcelableArrayListExtra(MapsUtil.REQUIRED);

        RecyclerView userListRecView = (RecyclerView) findViewById(R.id.ProfUtenteRecView);
        userListRecView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userListRecView.setLayoutManager(layoutManager);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        currentUser = ApiUtil.getUser(user.getUid());


        if (offert != null) {
            isOffert = true;
            Travel t = offert.get(0);
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put("travelId", t.id);
            TravelRequest[] mTravelRequests = ApiUtil.getTravelRequests(mHashMap);
            if (mTravelRequests.length == 1)
                startActivity(new Intent(UserTravelOffertRequestActivity.this, MapsActivity.class).putExtra(EMPTY, OFFERT));
            TravelRequest[] travelRequests = new TravelRequest[mTravelRequests.length - 1];
            User[] users = new User[mTravelRequests.length - 1];
            int j = 0;
            for (int i = 0; i < mTravelRequests.length; i++) {
                if (currentUser.id != mTravelRequests[i].userId) {
                    users[j] = ApiUtil.getUser(mTravelRequests[i].userId);
                    travelRequests[j] = mTravelRequests[i];
                    j++;
                }
            }

            AdapterUserTravelOfferti adapter = new AdapterUserTravelOfferti(users, travelRequests, getContext());
            userListRecView.setAdapter(adapter);


        } else if (required != null) {

            ArrayList<User> mUserArrayList = new ArrayList<>();
            ArrayList<Travel> mTravelReq = new ArrayList<>();
            for (int i = 0; i < required.size(); i++) {
                if (currentUser.id != required.get(i).ownerId) {
                    mUserArrayList.add(ApiUtil.getUser(required.get(i).ownerId));
                    mTravelReq.add(required.get(i));
                }
            }

            /*
            if (mUserArrayList.isEmpty()) {
                startActivity(new Intent(UserTravelOffertRequestActivity.this, MapsActivity.class).putExtra(EMPTY, REQUEST));
            }
            */

            AdapterUserTravelRichiesti adapter = new AdapterUserTravelRichiesti(mUserArrayList.toArray(new User[mUserArrayList.size()]), mTravelReq.toArray(new Travel[mTravelReq.size()]), getContext());
            userListRecView.setAdapter(adapter);

        }


        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);

        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = currentUser.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(currentUser.name + " " + currentUser.surname);

        super.onStart();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            if (isOffert)
                mtoolbar.setSubtitle(getResources().getString(R.string.offerUser));
            else
                mtoolbar.setSubtitle(getResources().getString(R.string.requestUser));

            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (currentUser.id == User.MOBILITY_DEMO || currentUser.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }
            if (currentUser.role == User.MOBILITY_MANAGER) {

                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }
                    return true;
                }
            });
        }
    });

    private Context getContext() {
        return this;
    }

}
