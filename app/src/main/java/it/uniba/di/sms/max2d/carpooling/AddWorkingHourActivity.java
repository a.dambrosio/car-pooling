package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.sql.Time;
import java.util.Calendar;
import java.util.List;

/**
 * Created by danie on 13/06/2018.
 */

public class AddWorkingHourActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    Toolbar mtoolbar;

    EditText editTextStart;
    EditText editTextEnd;
    Button buttonSave;
    Button buttonCancel;

    TextInputLayout textInputLayoutStart;
    TextInputLayout textInputLayoutEnd;



    String startWorkingHour;
    String endWorkingHour;
   final int RESULT_ANNULL = 5;


    private FirebaseUser FireBaseUser;
    private User user;
    String MESSAGGE_ERROR_FIELD;

    private Calendar calendar;

    TimePicker timePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_single_working_hour);

        editTextStart = (EditText)findViewById(R.id.editTextStartNewWorkingHour);
        editTextEnd = (EditText)findViewById(R.id.editTextEndNewWorkingHour);

        textInputLayoutStart = (TextInputLayout) findViewById(R.id.inputLayoutNewStart);
        textInputLayoutEnd = (TextInputLayout) findViewById(R.id.inputLayoutNewEnd);

        MESSAGGE_ERROR_FIELD =getResources().getString(R.string.messagge_error_field);


        FireBaseUser = FirebaseAuth.getInstance().getCurrentUser();
        user = ApiUtil.getUser(FireBaseUser.getUid());


        buttonSave = (Button) findViewById(R.id.buttonSaveNewWorkingHours);
        buttonSave.setOnClickListener(addNewWorkingHour);

        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(cancel);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        findViewById(R.id.addWorkingHourLayout).requestFocus();

        manageDatePicker(editTextStart);
        manageDatePicker(editTextEnd);

        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbarNew);


        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mtoolbar.setSubtitle(getResources().getString(R.string.manage_working_hour));

    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = user.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(user.name + " " + user.surname);

        super.onStart();
    }

    public View.OnClickListener addNewWorkingHour = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            startWorkingHour = editTextStart.getText().toString();
            endWorkingHour = editTextEnd.getText().toString();

            if(validate()) {


                WorkingHour wh = new WorkingHour(-1, startWorkingHour + ":00", endWorkingHour + ":00", user.companyId);
                if (ApiUtil.storeWorkingHour(wh)) {


                    Intent goBack = new Intent();
                    setResult(RESULT_OK, goBack);

                    finish();
                } else {
                    Intent goBack = new Intent();
                    setResult(RESULT_CANCELED, goBack);
                    finish();

                }

            }




        }
    };

    public View.OnClickListener cancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent goBack = new Intent();
            setResult(RESULT_ANNULL, goBack);
            finish();
        }


    };

    private void manageDatePicker(final EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if (isFocus == true) {


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker = new TimePickerDialog(AddWorkingHourActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        editText.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                    mTimePicker.setTitle(getResources().getString(R.string.select_time));
                mTimePicker.show();
            }

            }

        });

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker = new TimePickerDialog(AddWorkingHourActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        editText.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(getResources().getString(R.string.select_time));
                mTimePicker.show();
            }

            });
        }



        private boolean validate(){
            boolean valid = true;

            if(editTextStart.getText().toString().length()==0){
                valid = false;
                textInputLayoutStart.setError(MESSAGGE_ERROR_FIELD);
            }

            if(editTextEnd.getText().toString().length()==0){
                valid = false;
                textInputLayoutEnd.setError(MESSAGGE_ERROR_FIELD);
            }

            return valid;

        }


    @Override
    public void onBackPressed() {
        Intent goBack = new Intent();
        setResult(RESULT_ANNULL, goBack);
        finish();
    }


    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (user.id == User.MOBILITY_DEMO || user.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if(user.role==User.MOBILITY_MANAGER){
                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }

                    return true;
                }
            });
        }
    });

    private Context getContext(){
        return this;
    }


}
