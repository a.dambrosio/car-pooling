package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

public class UserListActivity extends AppCompatActivity {

    Toolbar mtoolbar;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    private FirebaseUser FireBaseUser;
    private User mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mUser = ApiUtil.getUser(currentUser.getUid());


        RecyclerView userListRecView = (RecyclerView) findViewById(R.id.ProfUtenteRecView);
        userListRecView.setHasFixedSize(true);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userListRecView.setLayoutManager(layoutManager);


        if (mUser != null) {


            User[] users = ApiUtil.getCompanyUsers(mUser.companyId);

            AdapterUserList adapter = new AdapterUserList(users, this);
            userListRecView.setAdapter(adapter);
        }

        mtoolbar = findViewById(R.id.my_toolbar);

        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = mUser.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(mUser.name + " " + mUser.surname);

        super.onStart();
    }

    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mtoolbar.setSubtitle(getResources().getString(R.string.user_list_activity));

            setSupportActionBar(mtoolbar);


            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (mUser.id == User.MOBILITY_DEMO || mUser.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if(mUser.role==User.MOBILITY_MANAGER){

                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));
                            finish();
                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }

                    return true;
                }
            });
        }
    });

    private Context getContext(){
        return this;
    }

}
