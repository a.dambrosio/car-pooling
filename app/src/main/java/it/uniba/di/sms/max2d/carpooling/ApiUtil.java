package it.uniba.di.sms.max2d.carpooling;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ApiUtil {

    public static final int CHECK_IF_USER_EXISTS = 0;
    public static final int NEW_USER = 1;
    public static final int USER_EXISTS = 0;
    public static final int GET_USER = 1;
    public static final int STORE_USER = 2;
    public static final int GET_ALL_COMPANIES = 3;
    public static final int GET_RANKING = 4;
    public static final int GET_WORKING_HOURS = 5;
    public static final int STORE_TRAVEL = 6;
    public static final int GET_TRAVELS = 7;
    public static final int GET_TRAVEL_REQUESTS = 8;
    public static final int UPDATE_USER = 9;
    public static final int GET_COMPANY = 10;
    public static final int GET_WORKING_HOUR = 11;
    public static final int DELETE_WORKING_HOUR = 13;
    public static final int STORE_WORKING_HOUR = 12;
    public static final int GET_COMPANY_USERS = 14;
    public static final int STORE_TRAVEL_REQUEST = 15;
    public static final int UPDATE_TRAVEL_REQUEST = 16;
    public static final int UPDATE_TRAVEL = 17;
    public static final int VERIFY_USER = 18;
    public static final int SEND_EMAIL = 19;

    private static final String BACKEND_URL = "http://smscarpooling.altervista.org/backend.php";
    private static String response;


    /**
     * Invia una richiesta di tipo POST al server web e riceve una stringa di risposta
     */
    public static String sendPostRequest(Map<String, Object> params) {
        try {
            URL url = new URL(BACKEND_URL);
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder result = new StringBuilder();

            for (int c; (c = in.read()) >= 0; )
                result.append((char) c);

            return result.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Controlla se l'utente è registrato nel database
     */
    public static boolean isUserRegistered(String userUid) {
        try {
            final String uid = userUid;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.CHECK_IF_USER_EXISTS);
                    params.put("uid", uid);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();
            JSONObject resp = new JSONObject(response);
            if (resp.has("result") && resp.getInt("result") == ApiUtil.NEW_USER) {
                // utente non registrato
                return false;
            } else if (resp.has("result") && resp.getInt("result") == ApiUtil.USER_EXISTS) {
                // l'utente è già registrato
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }


    /**
     * Restituisce un utente dal db a partire dal suo UID
     */
    public static User getUser(String userUid) {
        try {
            final String uid = userUid;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_USER);
                    params.put("uid", uid);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // utente trovato
                // resp contiene i dati dall'utente
                return new User(resp);
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Restituisce un utente dal db a partire dal suo id
     */
    public static User getUser(int userId) {
        try {
            final String[] responses = new String[1];
            final int id = userId;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_USER);
                    params.put("id", id);
                    responses[0] = ApiUtil.sendPostRequest(params);
                   // response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(responses[0]);

            if (! resp.has("error")) {
                // utente trovato
                // resp contiene i dati dall'utente
                return new User(resp);
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Memorizza un utente nel database ed eventualmente una nuova azienda, se l'utente è un Mobility Manager
     * @return true se l'inserimento è andato a buon fine, altrimenti false
     */
    public static boolean storeUser(final User user, final Company newCompany) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.STORE_USER);
                    params.put("user", user.toJsonObject());
                    if (user.role == User.MOBILITY_MANAGER && newCompany != null) {
                        params.put("newCompany", newCompany.toJsonObject());
                    }
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // utente inserito con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



    /**
     * Memorizza una working hour
     * @return true se l'inserimento è andato a buon fine, altrimenti false
     */
    public static boolean storeWorkingHour(final WorkingHour wh) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.STORE_WORKING_HOUR);
                    params.put("workingHour", wh.toJsonObject());
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // working hour inserita con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Cancella una working hour
     * @return true se la cancellazione è andata a buon fine, altrimenti false
     */
    public static boolean deleteWorkingHour(final int whId) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.DELETE_WORKING_HOUR);
                    params.put("whId", whId);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // working hour eliminata con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Aggiorna un utente
     * Come parametri è possibile passare "address" e "phone"
     * @return true se l'update è andato a buon fine, altrimenti false
     */
    public static boolean updateUser(final int userId, final Map<String, Object> newData) {
        try {

            final JSONObject newJsonData = new JSONObject();
            newJsonData.put("address", newData.get("address"));
            newJsonData.put("phone", newData.get("phone"));
            newJsonData.put("userId", userId);

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.UPDATE_USER);
                    params.put("user", newJsonData);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // utente aggiornato con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restituisce un'azienda dal db a partire dal suo id
     */
    public static Company getCompany(final int companyId) {
        try {
            final int id = companyId;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_COMPANY);
                    params.put("companyId", companyId);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // azienda trovata
                // resp contiene i dati dell'azienda
                return new Company(resp);
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Restituisce una mappa delle aziende registrate.Le chiavi sono i nomi delle aziende, i valori corrispondenti sono gli id.
     */
    public static HashMap<String, Integer> getAllCompanies() {
        HashMap<String, Integer> companies = new HashMap<>();
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_ALL_COMPANIES);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();
            JSONArray resp = new JSONArray(response);

            for (int i = 0; i < resp.length(); i++) {
                JSONObject company = resp.getJSONObject(i);
                companies.put(company.getString("nameCompany"), company.getInt("idCompany"));
            }

            return companies;

        } catch (Exception e) {
            e.printStackTrace();
            return companies;
        }
    }

    /**
     * Restituisce la classifica degli utenti ordinata per punti
     */
    public static JSONArray getRanking() {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_RANKING);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();
            JSONArray resp = new JSONArray(response);
            return resp;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Restituisce una mappa degli orari lavorativi dell'azienda specificata.Le chiavi sono le fasce orarie, i valori corrispondenti sono gli id.
     */
    public static LinkedHashMap<String, Integer> getWorkingHours(final int companyId) {
        LinkedHashMap<String, Integer> workingHours = new LinkedHashMap<>();
        final String[] responses = new String[1];
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_WORKING_HOURS);
                    params.put("company", companyId);
                    responses[0] = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();


            JSONArray resp = new JSONArray(responses[0]);

            for (int i = 0; i < resp.length(); i++) {
                JSONObject workingHour = resp.getJSONObject(i);
                String wh = workingHour.getString("startWH") + " - " + workingHour.getString("endWH");
                workingHours.put(wh, workingHour.getInt("idWH"));
            }

            return workingHours;

        } catch (Exception e) {
            e.printStackTrace();
            return workingHours;
        }
    }

    /**
     * Restituisce una WorkingHour a partire dal suo id
     */
    public static WorkingHour getWorkingHour(final int workingHourId) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_WORKING_HOUR);
                    params.put("workingHourId", workingHourId);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();


            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // trovata
                // resp contiene i dati
                return new WorkingHour(resp);
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Memorizza un passaggio nel database
     * @return true se l'inserimento è andato a buon fine, altrimenti false
     */
    public static boolean storeTravel(final Travel travel) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.STORE_TRAVEL);
                    params.put("travel", travel.toJsonObject());
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // passaggio inserito con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restituisce una lista di viaggi filtrati in base ai parametri passati
     * Filtri: ownerName, ownerSurname, type (intero), workingHour (id), date (d/m/Y), notExpired, ownerId, travelId, notDeleted
     */
    public static Travel[] getTravels(final Map<String, Object> filters) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_TRAVELS);
                    if (filters != null) {
                        for (String filter : filters.keySet()) {
                            params.put(filter, filters.get(filter));
                        }
                    }
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();
            JSONArray resp = new JSONArray(response);
            Travel[] travels = new Travel[resp.length()];

            for (int i=0; i<resp.length(); i++) {
                travels[i] = new Travel(resp.getJSONObject(i));
            }

            return travels;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Restituisce l'elenco di richieste relative ad un passaggio
     * Filtri: travelId, userId, notOwnerId, scoreNotZero, notDeleted
     */
    public static TravelRequest[] getTravelRequests(final Map<String, Object> filters) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_TRAVEL_REQUESTS);
                    if (filters != null) {
                        for (String filter : filters.keySet()) {
                            params.put(filter, filters.get(filter));
                        }
                    }

                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();


            JSONArray resp = new JSONArray(response);
            TravelRequest[] travelRequests = new TravelRequest[resp.length()];

            for (int i=0; i<resp.length(); i++) {
                travelRequests[i] = new TravelRequest(resp.getJSONObject(i));
            }

            return travelRequests;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Memorizza una travel request nel db
     * @return true se l'inserimento è andato a buon fine, altrimenti false
     */
    public static boolean storeTravelRequest(final TravelRequest travelRequest) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.STORE_TRAVEL_REQUEST);
                    params.put("travelRequest", travelRequest.toJsonObject());
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // inserimento con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Restituisce l'elenco di utenti di un'azienda
     */
    public static User[] getCompanyUsers(final int companyId) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.GET_COMPANY_USERS);
                    params.put("companyId", companyId);


                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONArray resp = new JSONArray(response);
            User[] users = new User[resp.length()];

            for (int i=0; i<resp.length(); i++) {
                users[i] = new User(resp.getJSONObject(i));
            }

            return users;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Aggiorna il punteggio di una travel request
     * Campi modificabili: newScore, newStatus, delete
     * @return true se l'update è andato a buon fine, altrimenti false
     */
    public static boolean updateTravelRequest(final int trId, final Map<String, Object> newFields) {
        try {

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.UPDATE_TRAVEL_REQUEST);
                    params.put("trId", trId);
                    if (newFields != null) {
                        for (String newField : newFields.keySet()) {
                            params.put(newField, newFields.get(newField));
                        }
                    }
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // aggiornato con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Aggiorna un passaggio
     * Campi modificabili: "newDate", "newWorkingHourId", "newCar", "newSeatsNum", "newType", "newWeekly"
     * @return true se l'update è andato a buon fine, altrimenti false
     */
    public static boolean updateTravel(final int travelId, final Map<String, Object> newFields) {
        try {

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.UPDATE_TRAVEL);
                    params.put("travelId", travelId);
                    if (newFields != null) {
                        for (String newField : newFields.keySet()) {
                            params.put(newField, newFields.get(newField));
                        }
                    }
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // aggiornato con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Verifica l'appartenenza di un utente all'azienda dichiarata in fase di registrazione
     * @return true se la verifica è andata a buon fine, altrimenti false
     */
    public static boolean verifyUserCompany(final int userId) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.VERIFY_USER);
                    params.put("userId", userId);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();

            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // verificato con successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Invia un'email
     * @return true, false
     */
    public static boolean sendEmail(final String to, final String subject, final String text) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> params = new LinkedHashMap<>();
                    params.put("request", ApiUtil.SEND_EMAIL);
                    params.put("to", to);
                    params.put("subject", subject);
                    params.put("txt", text);
                    response = ApiUtil.sendPostRequest(params);
                }
            });
            thread.start();
            thread.join();
            JSONObject resp = new JSONObject(response);

            if (! resp.has("error")) {
                // successo
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }




}
