package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class SearchFragment extends Fragment implements View.OnClickListener {

    public static final String DATA = "data";
    public static final String ORA = "ora";
    public static final String TRAGITTO = "tragitto";
    public static final String AUTONAME = "name";
    public static final String AUTOSURNAME = "surname";

    private EditText mEditData;
    private Spinner mEditOra;
    private RadioButton mCasaLavoro;
    private EditText mEditAutoName;
    private EditText mEditAutoSurname;
    private InteractionListener mListener;
    private LinkedHashMap<String, Integer> workingHourDatabase;

    private DatePickerDialog datePickerDialog;


    public interface InteractionListener {
        void onFragmentInteraction(HashMap<String, String> mhaHashMap);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int companyId = getArguments().getInt("azienda");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        mEditData = view.findViewById(R.id.EditTextData);
        mEditData.setOnFocusChangeListener(dataPick);
        mEditOra = view.findViewById(R.id.EditTextOra);

        workingHourDatabase = ApiUtil.getWorkingHours(companyId);
        ArrayAdapter<String> adapter = new ArrayAdapter<>((Activity) mListener, android.R.layout.simple_list_item_1, workingHourDatabase.keySet().toArray(new String[workingHourDatabase.size()]));
        mEditOra.setAdapter(adapter);


        mEditAutoName = view.findViewById(R.id.EditTextCar);
        mEditAutoSurname = view.findViewById(R.id.EditCognome);
        mCasaLavoro = view.findViewById(R.id.CasaLavoro);
        mCasaLavoro.setChecked(true);
        Button button = (Button) view.findViewById(R.id.fragment_send);
        button.setOnClickListener(this);
        return view;
    }

    public void sentString() {
        HashMap<String, String> mHashMap = new HashMap<>();
        if (mEditData.getText().toString().equalsIgnoreCase("")) {
            mHashMap.put(DATA, null);
        } else {
            mHashMap.put(DATA, mEditData.getText().toString());
        }
        if (mEditAutoName.getText().toString().equalsIgnoreCase("")) {
            mHashMap.put(AUTONAME, null);
        } else {
            mHashMap.put(AUTONAME, mEditAutoName.getText().toString());
        }
        if (mEditAutoSurname.getText().toString().equalsIgnoreCase("")) {
            mHashMap.put(AUTOSURNAME, null);
        } else {
            mHashMap.put(AUTOSURNAME, mEditAutoSurname.getText().toString());
        }
        mHashMap.put(ORA, String.valueOf(workingHourDatabase.get(mEditOra.getSelectedItem().toString())));


        if (mCasaLavoro.isChecked())
            mHashMap.put(TRAGITTO, "Casa-Lavoro");
        else
            mHashMap.put(TRAGITTO, "Lavoro-casa");

        mListener.onFragmentInteraction(mHashMap);

    }

    public static SearchFragment newInstance(Bundle a) {
        SearchFragment f = new SearchFragment();
        f.setArguments(a);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InteractionListener) {
            //init the listener
            mListener = (InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        sentString();
    }

    public View.OnFocusChangeListener dataPick = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus == true) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day


                datePickerDialog = new DatePickerDialog((Context) mListener,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                mEditData.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        }
    };

}
