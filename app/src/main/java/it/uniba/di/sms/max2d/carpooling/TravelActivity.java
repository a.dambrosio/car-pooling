package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import static android.content.RestrictionsManager.RESULT_ERROR;
import static android.content.RestrictionsManager.RESULT_ERROR_BAD_REQUEST;
import static com.facebook.FacebookSdk.getApplicationContext;

public class TravelActivity extends AppCompatActivity {


    private static final String ARG_SECTION_NUMBER = "section_number";
    String TRAVEL_ADDED ;
    private Toolbar mtoolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private User mUser;
    private boolean offert = false;
    private SearchFragment mSearchFragment;
    Handler handler;
    private Runnable mUpdateTimeTask;
    String TRAVEL_NOT_ADDED;
    User currentUser;
    private ProgressDialog mProgressDialog;


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;




    private final int REQUEST_CODE_EDIT_TRAVEL = 3;
    private final int REQUEST_CODE_INSERT_TRAVEL =2;
    private final int RESULT_NO_WORKING_HOUR = 4;
    private final int RESULT_INSERT_ERROR = 17;


    private final int RESULT_ANNULL =10;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        currentUser = ApiUtil.getUser(user.getUid());

        TRAVEL_ADDED =getResources().getString(R.string.travel_added);
        TRAVEL_NOT_ADDED = getResources().getString(R.string.travel_not_added);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        //setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the two
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent OpenViewInsertTravel = new Intent(TravelActivity.this,InsertTravel.class);
                startActivityForResult(OpenViewInsertTravel, REQUEST_CODE_INSERT_TRAVEL);
            }
        });

        threadToolbar.start();

        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mtoolbar.setSubtitle(getResources().getString(R.string.travels_activity));

    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage( getApplicationContext().getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                TravelActivity.this.finish();
            }
        });
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
    

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            return TraverlFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_INSERT_TRAVEL && resultCode == RESULT_OK) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.travel_added_succesfully), Snackbar.LENGTH_LONG);
            snackbar.show();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    reload();
                }
            }, 1500);
        } else if (requestCode == REQUEST_CODE_INSERT_TRAVEL && resultCode == RESULT_CANCELED) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.add_working_hour_annull), Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if(requestCode == REQUEST_CODE_INSERT_TRAVEL && resultCode == RESULT_ANNULL){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.add_working_hour_annull), Snackbar.LENGTH_LONG);
            snackbar.show();
        } else if(requestCode == REQUEST_CODE_INSERT_TRAVEL && resultCode == RESULT_INSERT_ERROR){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.add_travel_error), Snackbar.LENGTH_LONG);
            snackbar.show();
        }else if(requestCode == REQUEST_CODE_INSERT_TRAVEL && resultCode == RESULT_NO_WORKING_HOUR){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.no_working_hour), Snackbar.LENGTH_LONG);
            snackbar.show();
        }else if (requestCode == REQUEST_CODE_EDIT_TRAVEL && resultCode == RESULT_ERROR_BAD_REQUEST){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.travel_error_edit), Snackbar.LENGTH_LONG);
            snackbar.show();
        }else if (requestCode == REQUEST_CODE_EDIT_TRAVEL && resultCode == RESULT_OK){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getString(R.string.travel_edit_succesfully), Snackbar.LENGTH_LONG);
            snackbar.show();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    reload();
                }
            }, 2000);

        }else if(requestCode == REQUEST_CODE_EDIT_TRAVEL && resultCode == RESULT_ERROR){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getApplicationContext().getString(R.string.travel_not_added), Snackbar.LENGTH_LONG);
            snackbar.show();
        }else if (requestCode == REQUEST_CODE_EDIT_TRAVEL && resultCode == RESULT_ANNULL){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_content),getApplicationContext().getString(R.string.edit_annull), Snackbar.LENGTH_LONG);
            snackbar.show();
        }


    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = currentUser.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(currentUser.name + " " + currentUser.surname);

        super.onStart();
    }



    private void reload(){
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    /**
     * Thread per creazione e set toolbar
     */
    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar = (Toolbar) findViewById(R.id.my_toolbar);
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    /**
     * Thread per la creazione del navigation Drawer
     */

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (currentUser.id == User.MOBILITY_DEMO || currentUser.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if(currentUser.role==User.MOBILITY_MANAGER){

                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));
                            finish();
                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }

                    return true;
                }
            });
        }
    });

    /**
     * Funzione che permette di fare l'inflate per creare la gui
     * del menu della toolbar
     *
     * @param menu
     * @return
     */

    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;

        }

        return super.onOptionsItemSelected(item);
    }
    private Context getContext() {
        return this;
    }


}

