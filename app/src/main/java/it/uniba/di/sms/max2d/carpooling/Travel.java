package it.uniba.di.sms.max2d.carpooling;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class Travel implements Parcelable{

    public final static int HOME_WORK = 0;
    public final static int WORK_HOME = 1;

    public int id;
    public String date;
    public int workingHourId;
    public String car;
    public int seats;
    public int type;
    public int weeklyTravel;
    public int ownerId;

    public Travel (int id, String date, int workingHourId, int seats, String car, int type, int weeklyTravel, int ownerId) {
        this.id = id;
        this.date = date;
        this.workingHourId = workingHourId;
        this.car = car;
        this.type = type;
        this.weeklyTravel = weeklyTravel;
        this.ownerId = ownerId;
        this.seats = seats;
    }

    public Travel (JSONObject travel) {
        try {
            this.id = travel.getInt("idTravel");
            this.date = travel.getString("dateTravel");
            this.workingHourId = travel.getInt("FKWorkingHourTravel");
            this.car = travel.getString("carTravel");
            this.type = travel.getInt("typeTravel");
            this.weeklyTravel = travel.getInt("weeklyTravel");
            this.ownerId = travel.getInt("FKOwnerTravel");
            this.seats = travel.getInt("seatsTravel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJsonObject() {
        try {
            JSONObject travel = new JSONObject();
            travel.put("dateTravel", this.date);
            travel.put("FKWorkingHourTravel", this.workingHourId);
            travel.put("carTravel", this.car);
            travel.put("seatsTravel", this.seats);
            travel.put("typeTravel", this.type);
            travel.put("weeklyTravel", this.weeklyTravel);
            travel.put("FKOwnerTravel", this.ownerId);
            return travel;
        } catch (Exception e) {
            return null;
        }
    }

    public Travel(Parcel in) {

        this.id = in.readInt();
        this.date = in.readString();
        this.workingHourId = in.readInt();
        this.car = in.readString();
        this.seats = in.readInt();
        this.type = in.readInt();
        this.weeklyTravel = in.readInt();
        this.ownerId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(date);
        dest.writeInt(workingHourId);
        dest.writeString(car);
        dest.writeInt(seats);
        dest.writeInt(type);
        dest.writeInt(weeklyTravel);
        dest.writeInt(ownerId);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Travel createFromParcel(Parcel in) {
            return new Travel(in);
        }

        public Travel[] newArray(int size) {
            return new Travel[size];
        }
    };

}

