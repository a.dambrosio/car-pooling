package it.uniba.di.sms.max2d.carpooling;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class AuthActivity extends AppCompatActivity {

    // costanti
    private final int RC_SIGN_IN = 1;
    // oggetti
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mFbCallbackManager;
    private FirebaseAuth mAuth;
    private String response;
    // view
    private EditText loginEmailET;
    private EditText loginPasswordET;
    private ProgressDialog mProgressDialog;
    TextInputLayout textInputLayoutEmail;
    TextInputLayout textInputLayoutPassword;
    private boolean isGuest = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mAuth = FirebaseAuth.getInstance();

        // Gestione login account ospite
        Intent guestIntent = getIntent();
        if (guestIntent != null && guestIntent.hasExtra("id")) {
            isGuest = true;
            if (guestIntent.getIntExtra("id", User.MOBILITY_DEMO) == User.MOBILITY_DEMO) {
                signIn(User.MOBILITY_DEMO_EMAIL, User.DEMO_PASSWORD);
            } else if (guestIntent.getIntExtra("id", User.USER_DEMO) == User.USER_DEMO) {
                signIn(User.USER_DEMO_EMAIL, User.DEMO_PASSWORD);
            }
        }

        setupGoogleLogin();
        setupFacebookLogin();

        loginEmailET = (EditText) findViewById(R.id.loginEmailET);
        loginPasswordET = (EditText) findViewById(R.id.loginPasswordET);

        textInputLayoutEmail=(TextInputLayout) findViewById(R.id.inputLayoutEmailAuth);
        textInputLayoutPassword=(TextInputLayout) findViewById(R.id.inputLayoutPasswordAuth);

        Button loginLoginButton = (Button) findViewById(R.id.loginLoginButton);
        loginLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn(loginEmailET.getText().toString(), loginPasswordET.getText().toString());
                // Chiudi la tastiera per mostrare l'eventuale errore di login fallito
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        Button loginRegisterButton = (Button) findViewById(R.id.loginRegisterButton);
        loginRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToRegistration();
            }
        });



    }

    @Override
    public void onStart() {
        super.onStart();

        // Reindirizza chi è già autenticato
        redirectToSignInIfNotRegistered();

        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            showProgressDialog();
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task);
        }

        // Pass the activity result back to the Facebook SDK
        mFbCallbackManager.onActivityResult(requestCode, resultCode, data);

    }

    public void updateUI(FirebaseUser account) {
        if (account != null) {
            if (account.getDisplayName() != null) {
                Log.d("LOGIN", account.getDisplayName());
            }
        } else {
            Log.d("LOGIN", "No login");
        }
    }

    /**
     * Imposta il login standard
     */
    private void signIn(String email, String password) {
        Log.d("LOGIN", "signIn:" + email);
        if (!isGuest && !validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // Reindirizza alla pagina principale
                            onSuccessfulLogin();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("LOGIN", "signInWithEmail:failure", task.getException());
                            Snackbar.make(findViewById(R.id.loginLayout), getApplicationContext().getString(R.string.autentication_failed), Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        hideProgressDialog();

                    }
                });
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage( getApplicationContext().getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = loginEmailET.getText().toString();
        if (TextUtils.isEmpty(email)) {
            textInputLayoutEmail.setError( getApplicationContext().getString(R.string.error_field_required));
            valid = false;
        } else {
            textInputLayoutEmail.setError(null);
        }

        String password = loginPasswordET.getText().toString();
        if (TextUtils.isEmpty(password)) {
            textInputLayoutPassword.setError(getApplicationContext().getString(R.string.error_field_required));
            valid = false;
        } else {
            textInputLayoutPassword.setError(null);
        }

        return valid;
    }

    /**
     * Imposta il Google Login
     */
    private void setupGoogleLogin() {
        // Configure sign-in to request the userJson's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.google_server_client_id))
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        /*final GoogleSignInAccount googleAccount = GoogleSignIn.getLastSignedInAccount(this);
        Log.d("TEST", "Current Google token: " + googleAccount.getIdToken());*/
        SignInButton signInButton = (SignInButton) findViewById(R.id.googleSignInButton);
        signInButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                signInWithGoogle();
            }
        });
    }

    private void signInWithGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully
            firebaseAuthWithGoogle(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("LOGIN", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("LOGIN", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // Reindirizza alla pagina principale
                            onSuccessfulLogin();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("LOGIN", "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.loginLayout), getApplicationContext().getString(R.string.autentication_failed), Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        hideProgressDialog();

                        // ...
                    }
                });
    }

    /**
     * Imposta il Facebook Login
     */
    private void setupFacebookLogin() {
        mFbCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.fbSignInButton);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mFbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("LOGIN", "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d("LOGIN", "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("LOGIN", "facebook:onError", error);
                // ...
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("LOGIN", "handleFacebookAccessToken:" + token);

        showProgressDialog();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            // Reindirizza alla pagina principale
                            onSuccessfulLogin();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("LOGIN", "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.loginLayout), getApplicationContext().getString(R.string.autentication_failed), Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        hideProgressDialog();

                        // ...
                    }
                });
    }

    /**
     * Chiamata subito dopo il login
     */
    private void onSuccessfulLogin() {
       redirectToSignInIfNotRegistered();

           /*if (mAuth.getCurrentUser().getProviders().toString().contains("google")) {
               GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
               if (acct != null) {
                   String personName = acct.getDisplayName();
                   String personGivenName = acct.getGivenName();
                   String personFamilyName = acct.getFamilyName();
                   String personEmail = acct.getEmail();
                   String personId = acct.getId();
                   Log.d("info", personName + " " + personEmail + " " + personFamilyName + " " + personGivenName + " ");
               }
           } else if (mAuth.getCurrentUser().getProviders().toString().contains("facebook")) {
           }*/

    }

    /**
     * Reindirizza l'utente alla schermata di registrazione se non è registrato
     */
    private void redirectToSignInIfNotRegistered() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            return;
        }
        if (ApiUtil.isUserRegistered(mAuth.getCurrentUser().getUid())) {
            // L'utente è registrato, vai alla pagina principale
            AuthManager.redirectAuthenticatedToMainPage(this, true);
        } else {
            // L'utente ha effettuato il login con Facebook o Google, ma non è ancora registrato
            goToRegistration();
        }
    }

    /**
     * Vai alla pagina di registrazione
     */
    private void goToRegistration() {
        Intent goToReg = new Intent(AuthActivity.this, SignInActivity.class);
        startActivity(goToReg);
        finish();
    }


}
