package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AdapterUserList extends RecyclerView.Adapter<AdapterUserList.UserViewHolder> {
    private User[] users;
    Context mContext;
    LayoutInflater inflater;

    public AdapterUserList(User[] users, Context mContext) {
        this.users = users;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV;
        TextView notVerifiedTV;
        ImageView avatarImage;
        CardView cv;


        public UserViewHolder(View view) {
            super(view);
            nameTV = (TextView) view.findViewById(R.id.ProfileName);
            cv=(CardView) view.findViewById(R.id.card_viewProfilo);
            avatarImage = (ImageView) view.findViewById(R.id.ProfileAvatarImage);
            notVerifiedTV = (TextView) view.findViewById(R.id.notVerifiedTV);

        }


    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_list, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {
        try {
            final User user = users[position];
            Picasso avatarLoader = Picasso.get();
            final String name = user.name + " " + user.surname;

            holder.nameTV.setText(name);

            if (user.companyVerified == User.COMPANY_NOT_VERIFIED) {
                holder.nameTV.setTextColor(Color.parseColor("#A9A9A9"));
                holder.notVerifiedTV.setTextColor(Color.parseColor("#A9A9A9"));
                holder.notVerifiedTV.setVisibility(View.VISIBLE);

                holder.cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CharSequence options[] = new CharSequence[] {getApplicationContext().getResources().getString(R.string.verify_user)};
                        final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());


                        builder.setTitle(name);
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which == 0)
                                {
                                    if (ApiUtil.verifyUserCompany(user.id)) {
                                        reload();


                                    }

                                }

                            }
                        });

                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    }
                });

            } else {

                holder.cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent= new Intent(mContext, ProfileActivity.class);
                        intent.putExtra("userId", user.id);
                        mContext.startActivity(intent);


                    }
                });

            }


            if (user.avatarUrl.isEmpty()) {
                avatarLoader.load(R.drawable.default_image).into(holder.avatarImage);
            } else {
                //avatarLoader.load(avatarUrl).into(holder.avatarImage);
                avatarLoader.load(user.avatarUrl)
                        .into(holder.avatarImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap imageBitmap = ((BitmapDrawable) holder.avatarImage.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                                holder.avatarImage.setImageDrawable(imageDrawable);
                            }
                            @Override
                            public void onError(Exception $e) {
                                holder.avatarImage.setImageResource(R.drawable.default_image);
                            }
                        });
            }


        }  catch (Exception e) { e.printStackTrace(); }

    }

    private void reload(){
        ((Activity)mContext).finish();
        ((Activity)mContext).overridePendingTransition(0, 0);
        ((Activity)mContext).startActivity(((Activity) mContext).getIntent());
        ((Activity)mContext).overridePendingTransition(0, 0);

    }



    @Override
    public int getItemCount() {
        return users.length;
    }




}
