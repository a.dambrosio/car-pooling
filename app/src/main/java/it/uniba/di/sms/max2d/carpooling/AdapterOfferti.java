package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.Html;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class AdapterOfferti extends RecyclerView.Adapter<AdapterOfferti.PersonViewHolder> {
    private Travel[] travels;
    private Context context;
    private final int REQUEST_CODE_EDIT_TRAVEL = 3;
    private Map<Integer, WorkingHour> workingHours;
    private Map<Integer, TravelRequest[]> travelsRequests;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView data;
        TextView viaggio;
        TextView postiOccupati;
        TextView stato;
        ImageView img;


        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cvOfferti);

            data = (TextView) itemView.findViewById(R.id.dateOfferti);
            viaggio = (TextView) itemView.findViewById(R.id.viaggOfferti);

            stato = (TextView) itemView.findViewById(R.id.statOfferti);
            postiOccupati = (TextView) itemView.findViewById(R.id.postiOccupatiOfferti);
            img = (ImageView) itemView.findViewById(R.id.user_single_imageOfferti);
        }


    }

    AdapterOfferti(Travel[] travels, Context context) {
        this.travels = travels;
        this.context = context;
        this.workingHours = new LinkedHashMap<>();
        this.travelsRequests = new LinkedHashMap<>();

        for (int i=0; i<travels.length; i++) {
            Map<String, Object> filters = new HashMap<>();
            filters.put("travelId", travels[i].id);
            workingHours.put(i, ApiUtil.getWorkingHour(travels[i].workingHourId));
            travelsRequests.put(i, ApiUtil.getTravelRequests(filters));
        }

        ((TravelActivity)context).hideProgressDialog();
    }

    @Override
    public int getItemCount() {
        return travels.length;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offerti, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {

        try {


            final Travel travel = travels[i];
            String travelType;
            String hour = "";
            final boolean isExpiredTravel = DateUtil.isExpiredDate(DateUtil.formatToIta(travel.date));
            WorkingHour wh = workingHours.get(i);
            TravelRequest[] travelRequests = travelsRequests.get(i);


            if (travel.type == Travel.HOME_WORK) {
                travelType = getApplicationContext().getResources().getString(R.string.casalavoro);
                hour = wh.start;

            } else {
                travelType = getApplicationContext().getResources().getString(R.string.lavorocasa);
                hour = wh.end;
            }


            int waitingReqNum = 0;
            int takenSeats = 0;
            boolean finishedToday = false;

            for (TravelRequest travelRequest : travelRequests) {
                if (travelRequest.status == TravelRequest.PENDING_REQUEST) {
                    waitingReqNum++;
                } else if (travelRequest.status == TravelRequest.ACCEPTED_REQUEST) {
                    takenSeats++;
                }

                if (!finishedToday && travelRequest.score > 0) {
                    finishedToday = true;
                }
            }

            final boolean isFinishedToday = finishedToday;


            personViewHolder.data.setText(Html.fromHtml( " " + DateUtil.formatToIta(travel.date) + " - " + hour));
            personViewHolder.viaggio.setText(Html.fromHtml(" " + travelType));
            personViewHolder.postiOccupati.setText(Html.fromHtml( " " + takenSeats +" "+ getApplicationContext().getString(R.string.su) +" "+ travel.seats +" "+ getApplicationContext().getString(R.string.postiOccupati)));
            personViewHolder.stato.setText(Html.fromHtml( " " + waitingReqNum + " " + getApplicationContext().getResources().getQuantityString(R.plurals.pendingREQUEST, waitingReqNum)));

        if (isExpiredTravel || isFinishedToday) {
            personViewHolder.cv.setCardBackgroundColor(Color.rgb(232,232,232));
            personViewHolder.img.setImageResource(R.drawable.ic_directions_car_black_24dp);
            personViewHolder.stato.setText(Html.fromHtml("<b>" + getApplicationContext().getString(R.string.finished )+ " </b>"));
        } else if (waitingReqNum > 0) {
            personViewHolder.img.setImageResource(R.drawable.ic_directions_car_orange_24dp);
        }

            personViewHolder.cv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    CharSequence[] options = null;

                    if (isExpiredTravel || isFinishedToday) {
                        options = new CharSequence[]{getApplicationContext().getString(R.string.hide)};

                    } else {
                        options = new CharSequence[]{getApplicationContext().getString(R.string.edit)};
                    }

                    final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());


                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (which == 0) {
                                if (isExpiredTravel || isFinishedToday) {
                                    Map<String, Object> filters = new HashMap<>();
                                    filters.put("delete", 1);
                                    ApiUtil.updateTravel(travel.id, filters);

                                    ((Activity) context).finish();
                                    ((Activity) context).overridePendingTransition(0, 0);
                                    ((Activity) context).startActivity(((Activity) context).getIntent());
                                    ((Activity) context).overridePendingTransition(0, 0);

                                } else {
                                    Intent intent = new Intent(context, EditTravelActivity.class);
                                    intent.putExtra("idTravel", travel.id);
                                    //context.startActivity(intent);
                                    ((Activity) context).startActivityForResult(intent, REQUEST_CODE_EDIT_TRAVEL);
                                }


                            }

                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    return true;

                }
            });
            personViewHolder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isExpiredTravel && !isFinishedToday) {
                        Intent offerti = new Intent(getApplicationContext(), MapsActivity.class);
                        Bundle arg = new Bundle();
                        arg.putBoolean(MapsUtil.TRAVEL, true);
                        arg.putBoolean(MapsUtil.OFFERT, true);
                        arg.putParcelable("travel", travel);
                        offerti.putExtras(arg);
                        getApplicationContext().startActivity(offerti);
                    }


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}