package it.uniba.di.sms.max2d.carpooling;

import org.json.JSONObject;

public class Company {

    public static String COMPANY_DEMO_NAME = "Azienda srl";

    public String pIva;
    public int id;
    public String name;
    public String address;

    public Company (int id, String name, String address, String pIva) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.pIva = pIva;
    }

    public Company (JSONObject company) {
        try {
            this.id = company.getInt("idCompany");
            this.name = company.getString("nameCompany");
            this.address = company.getString("addressCompany");
            this.pIva = company.getString("pivaCompany");
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Restituisce un oggetto in formato JSON con i dati dell'azienda
     */
    public JSONObject toJsonObject() {
        try {
            JSONObject company = new JSONObject();
            company.put("nameCompany", this.name);
            company.put("addressCompany", this.address);
            company.put("pivaCompany", this.pIva);
            return company;
        } catch (Exception e) {
            return null;
        }
    }


}
