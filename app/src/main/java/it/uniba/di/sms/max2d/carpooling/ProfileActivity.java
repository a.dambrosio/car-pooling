package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    Toolbar mtoolbar;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mUser = ApiUtil.getUser(currentUser.getUid());


        if (getIntent() != null) {

            User user = ApiUtil.getUser(getIntent().getIntExtra("userId", -1));

            TextView dataNascita = (TextView) findViewById(R.id.dataNascita);
            TextView indirizzo = (TextView) findViewById(R.id.indirizzo);
            TextView nomeUtente=(TextView) findViewById(R.id.nomeUtente);
            final ImageView personPhoto = (ImageView) findViewById(R.id.user_Profile_List_image);

            dataNascita.setText(DateUtil.formatToIta(user.birthday));
            indirizzo.setText(user.address);
            nomeUtente.setText(user.name + " " + user.surname);

            Picasso avatarLoader = Picasso.get();

            if (user.avatarUrl.isEmpty()) {
                avatarLoader.load(R.drawable.default_image).into(personPhoto);
            } else {
                //avatarLoader.load(avatarUrl).into(holder.avatarImage);
                avatarLoader.load(user.avatarUrl)
                        .into(personPhoto, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap imageBitmap = ((BitmapDrawable) personPhoto.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                                personPhoto.setImageDrawable(imageDrawable);
                            }
                            @Override
                            public void onError(Exception $e) {
                                personPhoto.setImageResource(R.drawable.default_image);
                            }
                        });
            }





            RecyclerView profileRecView = (RecyclerView) findViewById(R.id.userListRecView);
            profileRecView.setHasFixedSize(true);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            profileRecView.setLayoutManager(layoutManager);

            HashMap<String, Object> filters = new HashMap<>();
            filters.put("userId", user.id);
            filters.put("scoreNotZero", true);
            TravelRequest[] travelRequests = ApiUtil.getTravelRequests(filters);

            AdapterProfileUser mAdapter = new AdapterProfileUser(travelRequests);
            profileRecView.setAdapter(mAdapter);


        }


        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);
        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = mUser.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(mUser.name + " " + mUser.surname);

        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (mUser.id == User.MOBILITY_DEMO || mUser.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }


            if(mUser.role==User.MOBILITY_MANAGER){
                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;

                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }

                    return true;
                }
            });
        }
    });

    private Context getContext(){
        return this;
    }
}
