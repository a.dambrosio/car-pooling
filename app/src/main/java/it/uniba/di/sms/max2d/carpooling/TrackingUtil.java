package it.uniba.di.sms.max2d.carpooling;



public class TrackingUtil {

    public static final int LOCATION_PERMISSIONS_REJECTED = 2;
    public static final int TRACKING_NOT_STARTED = 3;
    public static final int GPS_NOT_ENABLED = 4;

    public static final int TOO_FAR_ERROR = 5;
    public static final int DRIVER_BONUS = 10;
    public static final int BASE_SCORE = 5;

    /**
     * Calcola il punteggio da assegnare ai passeggeri
     * @param passengersNum (incluso l'autista)
     * @return
     */
    public static int calculatePassengerScore(int passengersNum) {
        if (passengersNum == 2) {
            return BASE_SCORE;
        } else if (passengersNum > 2) {
            return BASE_SCORE * passengersNum;
        }

        return 0;

    }

}
