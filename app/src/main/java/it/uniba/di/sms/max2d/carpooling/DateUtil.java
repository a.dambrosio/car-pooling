package it.uniba.di.sms.max2d.carpooling;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    /**
     * Accetta una stringa del tipo "yyyy-mm-dd" e restituisce una stringa nel formato italiano "dd/mm/yyyy"
     */
    public static String formatToIta(String date) {
        // Note, MM is months, not mm
        DateFormat outputFormat = new SimpleDateFormat("dd/mm/yyyy", Locale.ITALIAN);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.US);

        try {
            Date oldDate = inputFormat.parse(date);
            return outputFormat.format(oldDate);
        } catch (Exception e) {
            return "";
        }


    }


    /**
     * Controlla se la data è passatta
     * @param date in formato italiano
     * @return
     */
    public static boolean isExpiredDate(String date) {

        final Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR); // current year
        int currentMonth = c.get(Calendar.MONTH) + 1; // current month
        int currentDay = c.get(Calendar.DAY_OF_MONTH); // current day

        String insertData[] = date.split("/");

        int insertDay = Integer.parseInt(insertData[0]);
        int insertMonth = Integer.parseInt(insertData[1]);
        int insertYear = Integer.parseInt(insertData[2]);

        if (currentYear > insertYear) {
            return true;
        } else if (currentYear == insertYear && currentMonth > insertMonth) {
            return true;
        } else if (currentYear == insertYear && currentMonth == insertMonth && currentDay > insertDay) {
            return true;
        } else {
            return false;
        }

    }

}
