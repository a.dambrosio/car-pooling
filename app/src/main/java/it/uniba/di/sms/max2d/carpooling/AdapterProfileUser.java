package it.uniba.di.sms.max2d.carpooling;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.Html;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AdapterProfileUser extends RecyclerView.Adapter<AdapterProfileUser.UserProfileViewHolder> {
    TravelRequest[] travelRequests;

    public static class UserProfileViewHolder extends RecyclerView.ViewHolder {
        CardView cv;

        TextView tipo;
        TextView dataOra;
        TextView punti;


        UserProfileViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_user_list_profilo);
            tipo= (TextView) itemView.findViewById(R.id.Tipo);
            dataOra=(TextView) itemView.findViewById(R.id.DataOra);
            punti=(TextView) itemView.findViewById(R.id.Punti);

        }


    }


    AdapterProfileUser(TravelRequest[] travelRequests) {
        this.travelRequests = travelRequests;
    }

    @Override
    public int getItemCount() {
        return travelRequests.length;
    }

    @Override
    public UserProfileViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_profile_list, viewGroup, false);

        UserProfileViewHolder pvh = new UserProfileViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final UserProfileViewHolder userProfileViewHolder, int i) {
        TravelRequest travelRequest = travelRequests[i];
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("travelId", travelRequest.travelId);
        Travel travel = ApiUtil.getTravels(filters)[0];

        String travelType;

        if (travel.type == Travel.HOME_WORK) {
            travelType = getApplicationContext().getResources().getString(R.string.casalavoro);
        } else {
            travelType = getApplicationContext().getResources().getString(R.string.lavorocasa);
        }


        userProfileViewHolder.tipo.setText(Html.fromHtml(getApplicationContext().getResources().getString(R.string.type) + " " + travelType));
        userProfileViewHolder.dataOra.setText(Html.fromHtml(getApplicationContext().getResources().getString(R.string.Date) + " " + DateUtil.formatToIta(travel.date)));
        userProfileViewHolder.punti.setText(Html.fromHtml(getApplicationContext().getResources().getString(R.string.Score) + " " + travelRequest.score));


    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}