package it.uniba.di.sms.max2d.carpooling;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.signin.SignIn;


public class SplashActivity extends AppCompatActivity {

    private static final String TAG_LOG= SplashActivity.class.getName();

    private static final long MIN_WAIT_INTERVAL = 1500L; //intervallo minimo per il passaggio
    private static final long MAX_WAIT_INTERVAL = 3000L; //intervallo in cui il passaggio verrà in automatico
    //i due intervalli sono in millisecondi
    private static final int GO_AHEAD_WHAT=1;

    private long mStartTime; //istante della prima visualizzazione
    private boolean mIsDone; /* informazione relativo al fatto che il passaggio alla seconda attività è avvenuto
     la variabile mIsDOne può risultare utile qualora si voglia uscire dall'applicazione prima del passaggio
    all'attività successiva
    */

    private ProgressDialog mProgressDialog;

    private Handler mHandler =new Handler(){
        @Override
        public void handleMessage(Message msg){ //handleMessage è il metodo specializzato per la ricezione e invio dei messaggi
            switch (msg.what){ //il what viene specificato attraverso GO_HEAD_WHAT
                case GO_AHEAD_WHAT:
                    long elapsedTime = SystemClock.uptimeMillis() - mStartTime;
                    if(elapsedTime >= MIN_WAIT_INTERVAL && !mIsDone){
                        mIsDone=true;
                        /*
                        imposiamo a true la variabile qualoria sia trascorso aabbastanza tempo e soprattutto il passaggio all'attività
                        non sia già avvenuto o non si sia usciti prima.
                        dopo l'impostazione di mIsDone=true, invochiamo il metodo goAhead(), che contiene finalmente
                        il codice per passare all'attività principale dell'applicazione descritta appunto dalla classe MainActivity.
                         */
                        goAhead();
                    }
                    break;
            }
        }

    };

    private void goAhead() {
        /*hideProgressDialog();
        if (!MapsUtil.isOnline(this)) {
                finish();
        } else {*/
            final Intent intent = new Intent(this, MapsActivity.class); //INTENT ESPLICITO che permette l'attivazione di un componente
            startActivity(intent);
            finish();
        /*}*/
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage( getApplicationContext().getString(R.string.mandatory_connection));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                SplashActivity.this.finish();
            }
        });
        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        View decordView= getWindow().getDecorView();
        int uiOptios= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decordView.setSystemUiVisibility(uiOptios);

    }

    @Override
    protected void onStart(){
        super.onStart();

       /* if (!MapsUtil.isOnline(this)) {
            // Controllo connessione
            try {
                showProgressDialog();
            } catch (Exception e){}
        }*/

        mStartTime=SystemClock.uptimeMillis();
        final Message goAheadMessage=mHandler.obtainMessage(GO_AHEAD_WHAT);
        //sendMessageAtTime ci permette di inviare il messaggio in un determinano momento.
        mHandler.sendMessageAtTime(goAheadMessage, mStartTime + MAX_WAIT_INTERVAL);
        Log.d(TAG_LOG, "Handler message sent!");

    }
}
