package it.uniba.di.sms.max2d.carpooling;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.LinkedHashMap;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by danie on 13/06/2018.
 */

public class ManageCompanyActivity extends AppCompatActivity {

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    Toolbar mtoolbar;


    EditText editTextAddress;
    TextView editTextNameCompany;
    Spinner spinnerWorkingHours;
    Button buttonAddWornkingHour;
    Button buttonDeleteWorkingHour;
    Company company;
    private final int REQUEST_CODE_ADD_WORKING_HOUR = 3;
    private final int RESULT_ANNULL = 5;

    LinkedHashMap<String, Integer> workingHourDatabase;

    private FirebaseUser FireBaseUser;
    private User user;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_company);


        editTextNameCompany = (TextView) findViewById(R.id.textViewNameCompany);
        editTextAddress = (EditText) findViewById(R.id.editTextManageAddressCompany);
        spinnerWorkingHours = (Spinner) findViewById(R.id.spinnerManageWorkingHoursCompany);
        buttonAddWornkingHour = (Button) findViewById(R.id.buttonAddNewWorkingHours);
        buttonDeleteWorkingHour = (Button) findViewById(R.id.buttonDeleteWorkingHours);


        FireBaseUser = FirebaseAuth.getInstance().getCurrentUser();
        user = ApiUtil.getUser(FireBaseUser.getUid());

        company = ApiUtil.getCompany(user.companyId);

        workingHourDatabase = ApiUtil.getWorkingHours(user.companyId);

        buttonDeleteWorkingHour.setOnClickListener(deleteWorkingHour);

        buttonAddWornkingHour.setOnClickListener(addNewWorkingHour);


        if (getSupportActionBar() != null)
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        //


        initialize();
        manageWorkingHourSpinner();

        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbarManageCompany);


        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mtoolbar.setSubtitle(getResources().getString(R.string.manage_company));

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (spinnerWorkingHours.getSelectedItem() == null) {
            buttonDeleteWorkingHour.setVisibility(View.GONE);
        } else {
            buttonDeleteWorkingHour.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = user.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(user.name + " " + user.surname);

        super.onStart();
    }

    private void manageWorkingHourSpinner() {

        workingHourDatabase = ApiUtil.getWorkingHours(user.companyId);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.list_item, workingHourDatabase.keySet().toArray(new String[workingHourDatabase.size()]));

        spinnerWorkingHours.setAdapter(adapter);

    }

    private void initialize() {
        editTextNameCompany.setText(" " + company.name);

        editTextAddress.setEnabled(false);
        editTextAddress.setText(company.address);

    }

    public View.OnClickListener addNewWorkingHour = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent addWorkingHour = new Intent(ManageCompanyActivity.this, AddWorkingHourActivity.class);

            startActivityForResult(addWorkingHour, REQUEST_CODE_ADD_WORKING_HOUR);

        }

    };

    public View.OnClickListener deleteWorkingHour = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int idWorkingHours = getTimeSlot();
            if (ApiUtil.deleteWorkingHour(idWorkingHours)) {
                Snackbar snackbar = Snackbar.make(findViewById(R.id.ManageCompanyLayout), getApplicationContext().getString(R.string.elimination_done), Snackbar.LENGTH_LONG);
                snackbar.show();
                manageWorkingHourSpinner();

            }
            if (spinnerWorkingHours.getSelectedItem() == null)
                buttonDeleteWorkingHour.setVisibility(View.GONE);


        }

    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_ADD_WORKING_HOUR && resultCode == RESULT_OK) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.ManageCompanyLayout), getApplicationContext().getString(R.string.add_working_hour_done), Snackbar.LENGTH_LONG);
            snackbar.show();
            manageWorkingHourSpinner();
        } else if (requestCode == REQUEST_CODE_ADD_WORKING_HOUR && resultCode == RESULT_CANCELED) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.ManageCompanyLayout), getApplicationContext().getString(R.string.add_working_hour_error), Snackbar.LENGTH_LONG);


                /*
                Visualizza una snackBar come feedback per il corretto inseriemnto del passaggio
                 */
            snackbar.show();
        } else if (requestCode == REQUEST_CODE_ADD_WORKING_HOUR && resultCode == RESULT_ANNULL) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.ManageCompanyLayout), getApplicationContext().getString(R.string.add_working_hour_annull), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private int getTimeSlot() {
        String workingHourSelected = spinnerWorkingHours.getSelectedItem().toString();

        int id = workingHourDatabase.get(workingHourSelected);
        return id;

    }

    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.manage_company));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (user.id == User.MOBILITY_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if (user.role == User.MOBILITY_MANAGER) {

                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));
                            finish();
                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }

                    return true;
                }
            });
        }
    });

    private Context getContext() {
        return this;
    }


}
