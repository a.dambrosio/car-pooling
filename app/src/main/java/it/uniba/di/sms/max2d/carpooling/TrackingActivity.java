package it.uniba.di.sms.max2d.carpooling;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.DurationFieldType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.Duration;


public class TrackingActivity extends AppCompatActivity {

    private class Passenger {
        int id;
        int requestTravelId;
        Location location;

        Passenger(int id, int requestTravelId, Location location) {
            this.id = id;
            this.requestTravelId = requestTravelId;
            this.location = location;
        }

        void updateLocation(Location location) {
            this.location = location;
        }

    }

    private ArrayList<String> listItems=new ArrayList<String>();
    private ArrayAdapter<String> adapter;
    private ListView listView;
    private Travel travel;
    private TravelRequest travelRequest;
    private User user;
    private DatabaseReference thisTracking;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private boolean isOwner = false;
    private boolean isHomeWork = false;
    private boolean isStart = true;
    private final double MIN_DISTANCE = 1; //km
    private HashMap<Integer, Passenger> passengers = new HashMap<>(); // lista dei passeggeri
    private ArrayList<Passenger> winnerWorkHomePassengers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);

        ImageView rotateImage;

        rotateImage = (ImageView) findViewById(R.id.imageView);
        Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        rotateImage.startAnimation(startRotateAnimation);

        /**
         * Check permessi
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)

        {
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(android.Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions();
            } else {
                initTracking();
            }
        }


    }

    // Inizializza il tracking
    public void initTracking() {
        listView = (ListView) findViewById(R.id.trackingList);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        listView.setAdapter(adapter);

        // Utente loggato
        FirebaseUser usr = FirebaseAuth.getInstance().getCurrentUser();
        user = ApiUtil.getUser(usr.getUid());

        // Viaggio selezionato dall'utente
        travel = getIntent().getExtras().getParcelable("travel");
        Map<String, Object> f = new HashMap<>();
        f.put("travelId", travel.id);
        //travel = ApiUtil.getTravels(f)[0];

        // Associazione utente-travel
        Map<String, Object> filters = new HashMap<>();
        filters.put("travelId", travel.id);
        filters.put("userId", user.id);
        travelRequest = ApiUtil.getTravelRequests(filters)[0];

        final DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        // Referenza alla child all'interno di "tracking" rinominata con l'id del travel
        thisTracking = database.child("tracking").child(String.valueOf(travel.id));


        initFireBaseListener();
        initLocalization();


        // Register the listener with the Location Manager to receive location updates
        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        // Setta i flag
        isOwner = user.id == travel.ownerId;
        isHomeWork = travel.type == Travel.HOME_WORK;
        startFlow();


    }

    // Non appena un nuovo child viene aggiunto (ovvero un nuovo utente si associa al tracking), l'utente corrispondente
    // viene mostrato nella lista del tracking
    public void initFireBaseListener() {

        thisTracking.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // Nuovo utente collegato al tracking

                try {
                    User usr = ApiUtil.getUser(Integer.parseInt(dataSnapshot.getKey()));
                    HashMap<String, Object> usrInfo = (HashMap) dataSnapshot.getValue();
                    addItem(usr.name + " " + usr.surname);

                    if (isOwner && (usr.id != user.id)) {
                        Location currentLocation = new Location("newLocation");
                        currentLocation.setLatitude((double) usrInfo.get("lat"));
                        currentLocation.setLongitude((double) usrInfo.get("lon"));
                        passengers.put(usr.id, new Passenger(usr.id, ((Long) usrInfo.get("travelRequestId")).intValue(), currentLocation));
                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // La posizione di un utente è cambiata

                User usr = ApiUtil.getUser(Integer.parseInt(dataSnapshot.getKey()));
                HashMap<String, Object> usrInfo = (HashMap) dataSnapshot.getValue();

                if (isOwner && (usr.id != user.id)) {
                    if (dataSnapshot.hasChild("arrived")) {
                        winnerWorkHomePassengers.add(new Passenger(usr.id, ((Long) usrInfo.get("travelRequestId")).intValue(), null));
                    } else {
                        Location currentLocation = new Location("newLocation");
                        currentLocation.setLatitude((double) usrInfo.get("lat"));
                        currentLocation.setLongitude((double) usrInfo.get("lon"));
                        passengers.remove(usr.id);
                        passengers.put(usr.id, new Passenger(usr.id, ((Long) usrInfo.get("travelRequestId")).intValue(), currentLocation));
                    }
                }


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    // Calcolo distanza in km tra due location
    private static double distanceInMeter(final Location l1, Location l2) {
        return l1.distanceTo(l2) / 1000;
    }

    // Localizzazione dell'utente
    public void initLocalization() {
       // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.

                Log.d("TRACKING", "Nuova posizione rilevata");

                // CASA - LAVORO

                if (isOwner && isHomeWork && isStart) {
                    LatLng userAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, user.address);
                    Location addressLocation = new Location("newLocation");
                    addressLocation.setLatitude(userAddress.latitude);
                    addressLocation.setLongitude(userAddress.longitude);

                    // Verifica se il proprietario sta avviando il tracking da casa sua
                    if (distanceInMeter(location, addressLocation) <= MIN_DISTANCE) {
                        addOrUpdateUserInfoInDb(location);
                        Log.d("TRACKING", "Posizione aggiornata con successo");
                        isStart = false;
                    } else {
                        Log.d("TRACKING", "TOO FAR");
                        locationManager.removeUpdates(locationListener);
                        setResult(TrackingUtil.TOO_FAR_ERROR);
                        finish();
                    }

                } else if (isOwner && isHomeWork) {
                    Company company = ApiUtil.getCompany(user.companyId);
                    LatLng companyAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, company.address);
                    Location addressLocation = new Location("newLocation");
                    addressLocation.setLatitude(companyAddress.latitude);
                    addressLocation.setLongitude(companyAddress.longitude);

                    // Verifica se si è arrivati nei pressi della sede di lavoro
                    if (distanceInMeter(location, addressLocation) <= MIN_DISTANCE) {
                        locationManager.removeUpdates(locationListener);

                        Integer[] passengerIds = passengers.keySet().toArray(new Integer[passengers.size()]);

                        // Assegna i punti se il passeggero è ancora in auto

                        ArrayList<Passenger> winnerPassengers = new ArrayList<>();
                        for (Integer passengerId: passengerIds) {
                            Passenger passenger = passengers.get(passengerId);
                            if (distanceInMeter(location, passenger.location) <= MIN_DISTANCE) {
                                winnerPassengers.add(passenger);
                            }
                        }

                        for (Passenger passenger: winnerPassengers) {
                            HashMap<String, Object> fields = new HashMap<>();
                            fields.put("newScore", TrackingUtil.calculatePassengerScore(winnerPassengers.size() + 1));
                            ApiUtil.updateTravelRequest(passenger.requestTravelId, fields);
                        }

                        // Assegna i punti all'autista
                        if (winnerPassengers.size() > 0) {
                            HashMap<String, Object> fields = new HashMap<>();
                            fields.put("newScore", TrackingUtil.calculatePassengerScore(winnerPassengers.size() + 1) + TrackingUtil.DRIVER_BONUS);
                            ApiUtil.updateTravelRequest(travelRequest.id, fields);
                        }

                        // Imposta il viaggio come terminato
                        thisTracking.child("finished").setValue(true);
                        onTrackingFinished();


                    } else {
                        addOrUpdateUserInfoInDb(location);
                    }


                } else if (!isOwner && isHomeWork && isStart) {
                    final Location loc = location;
                    thisTracking.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            // Verifica che il proprietario abbia avviato il tracking
                            if (snapshot.hasChild(String.valueOf(travel.ownerId))) {

                                LatLng userAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, user.address);
                                Location addressLocation = new Location("newLocation");
                                addressLocation.setLatitude(userAddress.latitude);
                                addressLocation.setLongitude(userAddress.longitude);

                                if (distanceInMeter(loc, addressLocation) <= MIN_DISTANCE) {
                                    addOrUpdateUserInfoInDb(loc);
                                    isStart = false;

                                } else {
                                    Log.d("TRACKING", "TOO FAR");
                                    locationManager.removeUpdates(locationListener);
                                    setResult(TrackingUtil.TOO_FAR_ERROR);
                                    finish();
                                }
                            } else {
                                Log.d("TRACKING", "TRACKING NON AVVIATO");
                                locationManager.removeUpdates(locationListener);
                                setResult(TrackingUtil.TRACKING_NOT_STARTED);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else if (!isOwner && isHomeWork) {
                    addOrUpdateUserInfoInDb(location);

                    thisTracking.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            // Verifica che il viaggio sia terminato
                            if (snapshot.hasChild("finished")) {
                                Map<String, Object> filters = new HashMap<>();
                                filters.put("travelId", travel.id);
                                filters.put("userId", user.id);
                                travelRequest = ApiUtil.getTravelRequests(filters)[0];
                                onTrackingFinished();
                                locationManager.removeUpdates(locationListener);


                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                // LAVORO - CASA

                else if (isOwner && ! isHomeWork && isStart) {
                    Company company = ApiUtil.getCompany(user.companyId);
                    LatLng companyAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, company.address);
                    Location addressLocation = new Location("newLocation");
                    addressLocation.setLatitude(companyAddress.latitude);
                    addressLocation.setLongitude(companyAddress.longitude);

                    // Verifica se il proprietario sta avviando il tracking dalla sede di lavoro
                    if (distanceInMeter(location, addressLocation) <= MIN_DISTANCE) {
                        addOrUpdateUserInfoInDb(location);
                        Log.d("TRACKING", "Posizione aggiornata con successo");
                        isStart = false;
                    } else {
                        Log.d("TRACKING", "TOO FAR");
                        locationManager.removeUpdates(locationListener);
                        setResult(TrackingUtil.TOO_FAR_ERROR);
                        finish();
                    }

                }

                else if (isOwner && ! isHomeWork) {
                    LatLng userAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, user.address);
                    Location addressLocation = new Location("newLocation");
                    addressLocation.setLatitude(userAddress.latitude);
                    addressLocation.setLongitude(userAddress.longitude);

                    // Verifica se il proprietario è arrivato a casa
                    if (distanceInMeter(location, addressLocation) <= MIN_DISTANCE) {
                        locationManager.removeUpdates(locationListener);

                        // Assegna i punti se il passeggero è arrivato a casa

                        for (Passenger passenger: winnerWorkHomePassengers) {
                            HashMap<String, Object> fields = new HashMap<>();
                            fields.put("newScore", TrackingUtil.calculatePassengerScore(winnerWorkHomePassengers.size() + 1));
                            ApiUtil.updateTravelRequest(passenger.requestTravelId, fields);
                        }

                        // Assegna i punti all'autista
                        if (winnerWorkHomePassengers.size() > 0) {
                            HashMap<String, Object> fields = new HashMap<>();
                            fields.put("newScore", TrackingUtil.calculatePassengerScore(winnerWorkHomePassengers.size() + 1) + TrackingUtil.DRIVER_BONUS);
                            ApiUtil.updateTravelRequest(travelRequest.id, fields);
                        }

                        onTrackingFinished();


                    } else {
                        addOrUpdateUserInfoInDb(location);
                    }


                }

                else if (!isOwner && !isHomeWork && isStart) {
                    final Location loc = location;
                    thisTracking.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            // Verifica che il proprietario abbia avviato il tracking
                            if (snapshot.hasChild(String.valueOf(travel.ownerId))) {

                                Company company = ApiUtil.getCompany(user.companyId);
                                LatLng companyAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, company.address);
                                Location addressLocation = new Location("newLocation");
                                addressLocation.setLatitude(companyAddress.latitude);
                                addressLocation.setLongitude(companyAddress.longitude);

                                if (distanceInMeter(loc, addressLocation) <= MIN_DISTANCE) {
                                    addOrUpdateUserInfoInDb(loc);
                                    isStart = false;

                                } else {
                                    Log.d("TRACKING", "TOO FAR");
                                    locationManager.removeUpdates(locationListener);
                                    setResult(TrackingUtil.TOO_FAR_ERROR);
                                    finish();
                                }
                            } else {
                                Log.d("TRACKING", "TRACKING NON AVVIATO");
                                locationManager.removeUpdates(locationListener);
                                setResult(TrackingUtil.TRACKING_NOT_STARTED);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

                else if (!isOwner && !isHomeWork) {

                    LatLng userAddress = MapsUtil.getLocationFromAddress(TrackingActivity.this, user.address);
                    Location addressLocation = new Location("newLocation");
                    addressLocation.setLatitude(userAddress.latitude);
                    addressLocation.setLongitude(userAddress.longitude);

                    // Verifica se è arrivato a casa
                    if (distanceInMeter(location, addressLocation) <= MIN_DISTANCE) {
                        thisTracking.child(String.valueOf(user.id)).child("arrived").setValue(true);
                        locationManager.removeUpdates(locationListener);
                        onTrackingFinishedWorkHomePassenger();
                    } else {
                        addOrUpdateUserInfoInDb(location);
                    }

                }

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        try {
            locationManager.removeUpdates(locationListener);
            finish();

        } catch (Exception e) {
            finish();
        }

    }

    public void onTrackingFinished() {
        TextView trackingTV = (TextView) findViewById(R.id.trackingTV);
        TextView trackingUsersTV = (TextView) findViewById(R.id.trackingUserTV);

        Map<String, Object> filters = new HashMap<>();
        filters.put("travelId", travel.id);
        filters.put("userId", user.id);
        travelRequest = ApiUtil.getTravelRequests(filters)[0];

        trackingTV.setText(getApplicationContext().getResources().getString(R.string.tracking_activity));

        trackingUsersTV.setText(getApplicationContext().getResources().getString(R.string.complGuadag) + " "+  travelRequest.score + " " + getApplicationContext().getResources().getString(R.string.point));
        listItems.clear();
    }

    public void onTrackingFinishedWorkHomePassenger() {
        TextView trackingTV = (TextView) findViewById(R.id.trackingTV);
        TextView trackingUsersTV = (TextView) findViewById(R.id.trackingUserTV);

        Map<String, Object> filters = new HashMap<>();
        filters.put("travelId", travel.id);
        filters.put("userId", user.id);
        travelRequest = ApiUtil.getTravelRequests(filters)[0];

        trackingTV.setText(getApplicationContext().getResources().getString(R.string.tracking_activity));

        trackingUsersTV.setText(getApplicationContext().getResources().getString(R.string.complimentipunti));
        listItems.clear();
    }

    // Inserisce o aggiorna nel database di FireBase i dati dell'utente
    public void addOrUpdateUserInfoInDb(Location location) {
        final Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("lat", location.getLatitude());
        userInfo.put("lon", location.getLongitude());
        userInfo.put("travelRequestId", travelRequest.id);
        thisTracking.child(String.valueOf(user.id)).setValue(userInfo);
    }

    // Per aggiungere utenti alla lista dei tracciati
    public void addItem(String name) {
        listItems.add(name);
        adapter.notifyDataSetChanged();
    }

    // Differenzia il flow dell'activity in base ai diversi flag
    public void startFlow() {

            try {
                // Register the listener with the Location Manager to receive location updates
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            } catch (SecurityException e){

                finish();
            }

    }

    // Richiesta dei permessi di accesso alla posizione
    public void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(TrackingActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(TrackingActivity.this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(TrackingActivity.this,
                android.Manifest.permission.INTERNET)) {

            ActivityCompat.requestPermissions(TrackingActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.INTERNET},
                    2);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initTracking();
                } else {
                    setResult(TrackingUtil.LOCATION_PERMISSIONS_REJECTED);
                    finish();
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                setResult(TrackingUtil.GPS_NOT_ENABLED);
                finish();
            }
        } catch (Exception e) {}
    }
}
