package it.uniba.di.sms.max2d.carpooling;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends AppCompatActivity {

    private Toolbar mtoolbar;

    private HashMap<String, Integer> companies;
    private EditText editTextName;
    private EditText editTextSurname;
    private EditText editTextDate;
    private EditText editTextAddress;
    private EditText editTextTelephoneNumber;
    private AutoCompleteTextView autocompleteCompany;
    private EditText editTextEmail;
    private EditText editTextPassword1;
    private TextView textViewPassword2;
    private EditText editTextPassword2;
    private EditText editTextNewCompanyName;
    private EditText editTextNewCompanyAddress;
    private EditText editTextPIva;
    private DatePickerDialog datePickerDialog;
    private CheckBox checkBoxMobilityManager;
    private CheckBox checkBoxShowPassword;
    private LinearLayout mobilityManagerOptions;
    private LinearLayout normalUserOptions;
    private LinearLayout logInLayout;
    private Button buttonSignIn;

    private TextInputLayout textInputLayoutName,textInputLayoutSurname,textInputLayoutTelephone,textInputLayoutDate,textInputLayoutAddress,textInputLayoutCompany,
            textInputLayoutEmail,textInputLayoutPassword1,textInputLayoutPassword2,textInputLayoutNewCompany,textInputLayoutNewAddress,textInputLayoutNewPIVA;

    String MESSAGGE_ERROR_FIELD;
    String MESSAGGE_ERROR_PASSWORD;

    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        user = FirebaseAuth.getInstance().getCurrentUser();

        MESSAGGE_ERROR_FIELD =getResources().getString(R.string.messagge_error_field);
        MESSAGGE_ERROR_PASSWORD = getResources().getString(R.string.message_error_password);

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextSurname = (EditText) findViewById(R.id.editTextSurname);
        editTextDate = (EditText) findViewById(R.id.editTextDateOfBirth);
        editTextAddress = (EditText) findViewById(R.id.editTextUserAddress);
        autocompleteCompany = (AutoCompleteTextView) findViewById(R.id.autocompleteCompany);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword1 = (EditText) findViewById(R.id.editTextPassword1);
        //textViewPassword2 = (TextView) findViewById(R.id.textViewPassword2);
        editTextPassword2 = (EditText) findViewById(R.id.editTextPassword2);
        editTextNewCompanyAddress = (EditText) findViewById(R.id.editTextCompanyAddress);
        editTextPIva = (EditText) findViewById(R.id.editTextPIva);
        editTextTelephoneNumber = (EditText) findViewById(R.id.editTextTelephoneNumber);
        buttonSignIn = (Button) findViewById(R.id.buttonRegistration);
        editTextNewCompanyName = (EditText) findViewById(R.id.editTextCompanyName);

        textInputLayoutName = (TextInputLayout) findViewById(R.id.inputLayoutNameR);
        textInputLayoutSurname = (TextInputLayout) findViewById(R.id.inputLayoutSurnameR);
        textInputLayoutTelephone = (TextInputLayout) findViewById(R.id.inputLayoutPhoneNumberR);
        textInputLayoutDate = (TextInputLayout) findViewById(R.id.inputLayoutDateofBirthR);
        textInputLayoutAddress = (TextInputLayout) findViewById(R.id.inputLayoutAddressR);
        textInputLayoutCompany = (TextInputLayout) findViewById(R.id.inputLayoutCompanyR);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.inputLayoutEmailR);
        textInputLayoutPassword1 = (TextInputLayout) findViewById(R.id.inputLayoutPasswordR);
        textInputLayoutPassword2 = (TextInputLayout) findViewById(R.id.inputLayoutPassword2R);
        textInputLayoutNewCompany = (TextInputLayout) findViewById(R.id.inputLayoutNewCompanyNameR);
        textInputLayoutNewAddress = (TextInputLayout) findViewById(R.id.inputLayoutNewCompanyAddressR);
        textInputLayoutNewPIVA = (TextInputLayout) findViewById(R.id.inputLayoutPIvaR);





        closeKeyboard(getApplication().getBaseContext(), editTextDate.getWindowToken());
        // La nuova azienda non può avere lo stesso nome di una già esistente
        editTextNewCompanyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    // focus off
                    String str = editTextNewCompanyName.getText().toString();
                    if (companies.containsKey(str)) {
                        editTextNewCompanyName.setError("Questa azienda risulta già registrata");
                        editTextNewCompanyName.setText("");
                    }
                }
            }
        });


        /*
        Gestisce il datePicker nella casella data di nascita
         */
        manageDatePicker();

        /*
        Gestisce la visibilità delle opzioni aggiuntive del mobility manager
         */
        setVisibilityMobilityManagerOptions();

        /*
        Gestisce la visibilità dei caratteri nel campo password
         */
        setVisibilityPassword();

        /*
        Gestisce la visibilità dei campi email e password.
        Se la registazione viene effettuata con Google o Facobook, i campi non saranno visibili
         */
        setVisibilityEmailPassword();

        buttonSignIn.setOnClickListener(signIn);

        initAutocompleteCompany();

        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Inizializza l'autocomplete delle aziende
     */String my_var;
    private void initAutocompleteCompany() {
        // Mappa delle aziende registrate. Le chiavi sono i nomi delle aziende, i valori corrispondenti sono gli id.
        companies = ApiUtil.getAllCompanies();

        companies.remove(Company.COMPANY_DEMO_NAME);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.list_item, companies.keySet().toArray(new String[companies.size()]));
        autocompleteCompany.setAdapter(adapter);
        autocompleteCompany.setThreshold(1);


        // L'utente deve poter scegliere solo tra le aziende registrate
        autocompleteCompany.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    // focus off
                    String str = autocompleteCompany.getText().toString();
                    if ( ! companies.containsKey(str)) {
                        autocompleteCompany.setText("");
                    }
                }
            }
        });


    }

    /**
     * Memorizza l'utente nel database ed eventualmente la nuova azienda
     */
    private void storeUser(final User usr, Company newCompany) {
        if(ApiUtil.storeUser(usr, newCompany)) {
            // Utente registrato con successo
            if (usr.role == User.SIMPLE_USER) {

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            User[] users = ApiUtil.getCompanyUsers(ApiUtil.getCompany(usr.companyId).id);
                            User owner = null;

                            for (User usr: users) {
                                if (usr.role == User.MOBILITY_MANAGER) {
                                    owner = usr;
                                    break;
                                }
                            }

                            if (ApiUtil.sendEmail(owner.email, String.format(getResources().getString(R.string.email_subject)), String.format(getResources().getString(R.string.email_text), usr.name + " " + usr.surname))) {
                                Log.d("EMAIL", "ok");
                            }
                        } catch (Exception e) {
                            Log.d("EMAIL", "no");
                        }
                    }
                });
                thread.start();
            }
            Intent goToMainPage = new Intent(SignInActivity.this, MapsActivity.class);
            goToMainPage.putExtra("loginSuccessful", true);
            startActivity(goToMainPage);
            finish();
        } else {
            Snackbar.make(findViewById(R.id.signInLayout), getResources().getString(R.string.registration_error), Snackbar.LENGTH_SHORT).show();
        }
    }

    /*
    Inserisci un nuovo passaggio nel database cliccando sul punsante
     */
    public View.OnClickListener signIn = new View.OnClickListener() {

        String userName;
        String userSurname;
        String birthDate;
        String userAddress;
        String company;
        String email;
        String password1;
        String newCompanyName;
        String newCompanyAddress;
        String pIva;
        String telephone;
        int role;
        int userCompany;
        int companyVerified;
        Company newCompany;


        @Override
        public void onClick(View v) {

            boolean valid = validate();


            if (valid) {
                userName = getUserName();
                userSurname = getUserSurname();
                birthDate = getBirthDate();
                userAddress = getUserAddress();
                company = getCompany();
                telephone = getTelephone();
                email = getEmail();
                password1 = getPassword1();
                newCompanyName = getNewCompanyName();
                newCompanyAddress = getNewCompanyAddress();
                pIva = getPIva();



                if (checkBoxMobilityManager.isChecked()) {
                    role = User.MOBILITY_MANAGER;
                    companyVerified = User.COMPANY_VERIFIED;
                    userCompany = -1; // non sarà considerata, poichè verrà associata la nuova azienda che l'utente ha creato
                    newCompany = new Company(-1, newCompanyName, newCompanyAddress, pIva);

                } else {
                    role = User.SIMPLE_USER;
                    companyVerified = User.COMPANY_NOT_VERIFIED;
                    userCompany = companies.get(company);
                    newCompany = null; // non crea una nuova azienda, ma si iscrive ad una già esistente
                }

                if (user != null) {
                    // Accesso effettuato con Google o Facebook
                    String photoUrl = user.getPhotoUrl() != null ? user.getPhotoUrl().toString() : "";
                    User usr = new User(-1, user.getUid(), email.trim(), userName.trim(), userSurname.trim(),
                            userCompany, role, birthDate.trim(), userAddress.trim(), photoUrl, telephone.trim(), companyVerified);

                    storeUser(usr, newCompany);

                } else {
                    // Registrazione standard email/password
                    // Memorizza nel db e in Firebase
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password1)
                            .addOnCompleteListener(SignInActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        user = FirebaseAuth.getInstance().getCurrentUser();
                                        User usr = new User(-1, user.getUid(), email.trim(), userName.trim(), userSurname.trim(),
                                                userCompany, role, birthDate.trim(), userAddress.trim(), "", telephone.trim(), companyVerified);
                                        storeUser(usr, newCompany);
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        String error = null;
                                        try
                                        {
                                            throw task.getException();
                                        }
                                        // if user enters wrong email.
                                        catch (FirebaseAuthWeakPasswordException weakPassword)
                                        {
                                            error = weakPassword.getReason();
                                        }
                                        // if user enters wrong password.
                                        catch (FirebaseAuthInvalidCredentialsException malformedEmail)
                                        {
                                            error = malformedEmail.getMessage();
                                        }
                                        catch (FirebaseAuthUserCollisionException existEmail)
                                        {
                                            error = existEmail.getMessage();
                                        }
                                        catch (Exception e)
                                        {
                                            error = e.getMessage();
                                        }
                                        Snackbar.make(findViewById(R.id.signInLayout), error, Snackbar.LENGTH_SHORT).show();
                                    }

                                }
                            });
                }




            }



        }

    };

    /*
   Restituisce il nome dell'utente
    */
    private String getUserName() {
        return editTextName.getText().toString();
    }

    /*
  Restituisce il cognome dell'utente
   */
    private String getUserSurname() {
        return editTextSurname.getText().toString();
    }

    /*
  Restituisce la data indicata dall'utente
   */
    private String getBirthDate() {
        return editTextDate.getText().toString();
    }

    /*
 Restituisce l'indirizzo dell'utente
  */
    private String getUserAddress() {
        return editTextAddress.getText().toString();
    }

    /*
 Restituisce l'azienda selezionata dall'utente
 L'azienda è già presente nello spinner
  */
    private String getCompany() {
        return autocompleteCompany.getText().toString();
    }

    /*
 Restituisce l'e-mail dell'utente
  */
    private String getEmail() {
        if (user != null) {
            return user.getEmail();
        }
        return editTextEmail.getText().toString();
    }

    /*
 Restituisce la password dell'utente
  */
    private String getPassword1() {
        return editTextPassword1.getText().toString();
    }

    /*
Restituisce la password ripetuta dell'utente
*/
    private String getPassword2() {
        return editTextPassword2.getText().toString();
    }


    private String getTelephone(){
        return editTextTelephoneNumber.getText().toString();
    }


    /*
Restituisce il nome della nuova azieda indicata dall'utente
L'utente ha selezionato la modalità MobilityMananger
*/
    private String getNewCompanyName() {
        return editTextNewCompanyName.getText().toString();
    }

    /*
Restituisce l'indirizzo della nuova azieda indicata dall'utente
L'utente ha selezionato la modalità MobilityMananger
*/
    private String getNewCompanyAddress() {
        return editTextNewCompanyAddress.getText().toString();
    }

    /*
 Restituisce la PIVA della nuova azieda indicata dall'utente
 L'utente ha selezionato la modalità MobilityMananger
 */
    private String getPIva() {
        return editTextPIva.getText().toString();
    }



    /*
    Aggiunge vincoli di inseriemento ai campi presenti nell'activity
     */
    private boolean validate() {
        Boolean correctInsert = true;

        if (getUserName().length() <= 0) {
            correctInsert = false;
            textInputLayoutName.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getUserSurname().length() <= 0) {
            correctInsert = false;
            textInputLayoutSurname.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getBirthDate().length() <= 0) {
            correctInsert = false;
            textInputLayoutDate.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getUserAddress().length() <= 0) {
            correctInsert = false;
            textInputLayoutAddress.setError(MESSAGGE_ERROR_FIELD);
        }

        if (!checkBoxMobilityManager.isChecked()) {
            if (getCompany().length() <= 0) {
                correctInsert = false;
                textInputLayoutCompany.setError(MESSAGGE_ERROR_FIELD);
            }
        }

        if (user == null) {


        if (getEmail().length() <= 0) {
            correctInsert = false;
            textInputLayoutEmail.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getPassword1().length() <= 0) {
            correctInsert = false;
            textInputLayoutPassword1.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getPassword2().length() <= 0) {
            correctInsert = false;
            textInputLayoutPassword2.setError(MESSAGGE_ERROR_FIELD);
        }


        /*
        Controlla che le password inserita dall'utente coincida nei due campi previsti
         */
        if (getPassword1().length() > 0 && getPassword2().length() > 0) {
            if (!getPassword1().equals(editTextPassword2.getText().toString())) {
                correctInsert = false;
                textInputLayoutPassword1.setError(MESSAGGE_ERROR_PASSWORD);
                textInputLayoutPassword2.setError(MESSAGGE_ERROR_PASSWORD);
                editTextPassword1.setText("");
                editTextPassword2.setText("");
            }
        }
    }


        if(checkBoxMobilityManager.isChecked()){
            if (getNewCompanyName().length() <= 0) {
                correctInsert = false;
                textInputLayoutNewCompany.setError(MESSAGGE_ERROR_FIELD);
            }

            if (getNewCompanyAddress().length() <= 0) {
                correctInsert = false;
                textInputLayoutNewAddress.setError(MESSAGGE_ERROR_FIELD);
            }

            if (getPIva().length() <= 0) {
                correctInsert = false;
                textInputLayoutNewPIVA.setError(MESSAGGE_ERROR_FIELD);
            }
        }

        if(getTelephone().length() <=0){
            correctInsert = false;
            textInputLayoutTelephone.setError(MESSAGGE_ERROR_FIELD);

        }

        return correctInsert;
    }




        /*
        Gestisce il datePicker nella casella data di nascita
        */
    private void manageDatePicker() {
        closeKeyboard(getApplication().getBaseContext(), editTextDate.getWindowToken());
        editTextDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if (isFocus == false) {
                    if (editTextDate.length() > 0)
                        editTextDate.setError(null);
                    else
                        editTextDate.setError(MESSAGGE_ERROR_FIELD);
                }
                if (isFocus == true) {
                    // calender class's instance and get current date , month and year from calender
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                    closeKeyboard(getApplication().getBaseContext(), editTextDate.getWindowToken());


                    datePickerDialog = new DatePickerDialog(SignInActivity.this, new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            // set day of month , month and year value in the edit text
                            editTextDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);
                    datePickerDialog.show();

                }
            }
        });
        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                closeKeyboard(getApplication().getBaseContext(), editTextDate.getWindowToken());
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                datePickerDialog = new DatePickerDialog(SignInActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        editTextDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year));
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();


            }
        });
    }


             /*
           Gestisce la visibilità delle opzioni aggiuntive del mobility manager
            */
    private void setVisibilityMobilityManagerOptions(){

        checkBoxMobilityManager = (CheckBox) findViewById(R.id.checkBoxMobilityManager);
        mobilityManagerOptions = (LinearLayout) findViewById(R.id.mobilityManagerLayout);
        normalUserOptions = (LinearLayout) findViewById(R.id.normalUserLayout);


        initialVisibility(mobilityManagerOptions,View.GONE,normalUserOptions,View.VISIBLE);


        checkBoxMobilityManager.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(checkBoxMobilityManager.isChecked()){
                    mobilityManagerOptions.setVisibility(View.VISIBLE);
                    normalUserOptions.setVisibility(View.GONE);
                }else{
                    mobilityManagerOptions.setVisibility(View.GONE);
                    normalUserOptions.setVisibility(View.VISIBLE);
                }
            }
        });;
    }

    /*
       Gestisce la visibilità dei caratteri nel campo password
        */
    private void setVisibilityPassword(){

        checkBoxShowPassword = (CheckBox) findViewById(R.id.checkBoxShowPassword);

        final int inputType = editTextPassword1.getInputType();

        checkBoxShowPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(checkBoxShowPassword.isChecked()){
                    editTextPassword1.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    editTextPassword2.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    editTextPassword1.setInputType(inputType);
                    editTextPassword2.setInputType(inputType);

                }
            }
        });;
    }




        /*
        Gestisce la visibilità dei campi email e password.
        Se la registazione viene effettuata con Google o Facobook, i campi non saranno visibili
         */
    private void setVisibilityEmailPassword(){

        //user null registrazione standard
        //user not null registrazione con Google o Facebook

        logInLayout = (LinearLayout) findViewById(R.id.logInLayout);

        if(user != null){
            logInLayout.setVisibility(View.GONE);
        }

    }

            /*
           Configura la visibilità delle opzioni del mobilityManager e di un normale utente
           Al primo accesso sarà visibile solo la modalità "normale utente
            */
    private void initialVisibility(LinearLayout layout1,int visibilityLayout1,LinearLayout layout2,int visibilityLayout2 ){
        layout1.setVisibility(visibilityLayout1);
        layout2.setVisibility(visibilityLayout2);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AuthManager.logout(this);
    }
    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar = (Toolbar) findViewById(R.id.my_toolbar);
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            setSupportActionBar(mtoolbar);

        }
    });
}
