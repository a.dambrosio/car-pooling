package it.uniba.di.sms.max2d.carpooling;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

public class AdapterRanking extends RecyclerView.Adapter<AdapterRanking.RankingViewHolder> {
    private JSONArray ranking;
    Context mContext;
    LayoutInflater inflater;

    public AdapterRanking(JSONArray ranking, Context mContext) {
        this.ranking = ranking;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }


    public class RankingViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV;
        ImageView avatarImage;
        TextView positionTV;
        TextView scoreTV;

        public RankingViewHolder(View view) {
            super(view);
            nameTV = (TextView) view.findViewById(R.id.rankingNameTV);
            positionTV = (TextView) view.findViewById(R.id.rankingPositionTV);
            avatarImage = (ImageView) view.findViewById(R.id.rankingAvatarImage);
            scoreTV = (TextView) view.findViewById(R.id.rankingScoreTV);
        }


    }

    @Override
    public RankingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ranking, parent, false);
        return new RankingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RankingViewHolder holder, final int position) {
        try {
            JSONObject user = ranking.getJSONObject(position);
            String name = user.getString("nameUser") + " " + user.getString("surnameUser");
            String avatarUrl = user.getString("avatarUser");
            Picasso avatarLoader = Picasso.get();

            holder.nameTV.setText(name);
            holder.positionTV.setText(String.valueOf(position + 1));
            holder.scoreTV.setText(user.getString("scoreUser") + "pti");

            if (avatarUrl.isEmpty()) {
                avatarLoader.load(R.drawable.default_image).into(holder.avatarImage);
            } else {
                //avatarLoader.load(avatarUrl).into(holder.avatarImage);
                avatarLoader.load(avatarUrl)
                        .into(holder.avatarImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap imageBitmap = ((BitmapDrawable) holder.avatarImage.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                                holder.avatarImage.setImageDrawable(imageDrawable);
                            }
                            @Override
                            public void onError(Exception $e) {
                                holder.avatarImage.setImageResource(R.drawable.default_image);
                            }
                        });
            }

        }  catch (Exception e) { e.printStackTrace(); }

    }

    @Override
    public int getItemCount() {
        return ranking.length();
    }


}
