package it.uniba.di.sms.max2d.carpooling;

import android.Manifest;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;

import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.maps.GeoApiContext;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, SearchFragment.InteractionListener {

    private Travel mTravel;
    public static final int EMPTY = 0;
    private User mUser;
    private Toolbar mtoolbar;
    private SearchFragment mSearchFragment;
    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private SupportMapFragment mapFragment;
    private FloatingActionButton mPosition;
    private DrawerLayout mDrawerLayout;
    private Bundle dati;
    private NavigationView mNavigationView;
    private Travel[] mTravels;
    private int click = 0;
    private boolean externalUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        try {
            // Ottenere una istanza di LocationManager
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            // Ottenere una istanza di SupportMapFragment
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);

            mapFragment.getMapAsync(this);

            //Ottenere un riferimento al fab
            mPosition = findViewById(R.id.GPS);

            FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            //Verifica se l'utente è registrato
            if (mFirebaseUser == null)
                externalUser = true;
            if (externalUser == false) {
                mUser = ApiUtil.getUser(mFirebaseUser.getUid());

                //verifica se l'utente è stato verificato dall'azienda
                if (mUser.companyVerified == User.COMPANY_VERIFIED)
                    mPosition.setVisibility(View.GONE);
            }

            //L'activity può ricevere diverse richieste
            Intent mIntent = getIntent();
            dati = mIntent.getExtras();

            String empty = dati.getString(UserTravelOffertRequestActivity.EMPTY);

            //Stringa ricevuta dall'activity UserTravelOffertRequestActivity
            //indica l'assenza di richieste
            if (empty != null) {
                if (empty.equals(UserTravelOffertRequestActivity.OFFERT))
                    Snackbar.make(mPosition, getApplicationContext().getString(R.string.no_one_added), Snackbar.LENGTH_LONG).show();
            }

            //thread per la creazione della toolbar
            threadToolbar.start();
            try {
                threadToolbar.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            /**
             * Check permessi
             */

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)

            {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                    request();
                } else {
                    searchPosition();
                }
            }
        } catch (Exception e) {
            // Connessione non ancora pronta
            AuthManager.logout(this);
        }

    }


    @Override
    protected void onStart() {
        if (externalUser != true) {
            final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
            TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);


            String avatarUrl = mUser.avatarUrl;
            Picasso avatarLoader = Picasso.get();

            if (avatarUrl.isEmpty()) {
                avatarLoader.load(R.drawable.default_image).into(profilImage);
            } else {
                avatarLoader.load(avatarUrl)
                        .into(profilImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                                profilImage.setImageDrawable(imageDrawable);
                            }

                            @Override
                            public void onError(Exception $e) {
                                profilImage.setImageResource(R.drawable.default_image);
                            }
                        });
            }

            profilName.setText(mUser.name + " " + mUser.surname);
        }
        super.onStart();
    }


    public void request() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this,
                Manifest.permission.INTERNET)) {

            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.INTERNET},
                    2);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }


    /**
     * Permette l'ineterazione con il Fragment search
     */

    public void onFragmentInteraction(HashMap<String, String> mhaHashMap) {
        MapsUtil.removeFragment((AppCompatActivity) getContext(), MapsUtil.SEARCH);
        Map<String, Object> filters = new HashMap<>();
        if (mhaHashMap.get(SearchFragment.DATA) != null)
            filters.put("date", mhaHashMap.get(SearchFragment.DATA));
        if (mhaHashMap.get(SearchFragment.AUTONAME) != null) {
            filters.put("ownerName", mhaHashMap.get(SearchFragment.AUTONAME));
        }
        if (mhaHashMap.get(SearchFragment.AUTOSURNAME) != null) {
            filters.put("ownerSurname", mhaHashMap.get(SearchFragment.AUTOSURNAME));
        }

        if (mhaHashMap.get(SearchFragment.TRAGITTO) != null) {
            if (mhaHashMap.get(SearchFragment.TRAGITTO).equalsIgnoreCase("Casa-Lavoro")) {
                filters.put("type", Travel.HOME_WORK);
            } else filters.put("type", Travel.WORK_HOME);
        }

        filters.put("workingHour", Integer.valueOf(mhaHashMap.get(SearchFragment.ORA)));

        Log.d("DATE", DateUtil.formatToIta(mhaHashMap.get(SearchFragment.DATA)));

        Travel[] travels = ApiUtil.getTravels(filters);
        mTravels = travels;

        mMap.clear();

        mPosition.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_purple)));
        mPosition.setImageResource(R.drawable.ic_add_white_24dp);
        mPosition.setOnClickListener(onClickToRequest);

        int cont = 0;
        Company mCompany = ApiUtil.getCompany(mUser.companyId);
        MapsUtil.addMarkerPositionStandard(this, mMap, MapsUtil.getLocationFromAddress(this, mUser.address), getLayoutInflater(), mUser.address);
        MapsUtil.addMarkerPositionStandard(this, mMap, MapsUtil.getLocationFromAddress(this, mCompany.address), getLayoutInflater(), mCompany.address);

        if (travels != null) {
            for (Travel t : travels) {
                User u = ApiUtil.getUser(t.ownerId);
                if (mUser.id != u.id) {
                    MapsUtil.addMarkerToRequest(this, mMap, MapsUtil.getLocationFromAddress(this, u.address), getLayoutInflater(), u);
                    cont++;
                }
            }
        }


        CameraPosition cameraPosition = MapsUtil.setCamera(MapsUtil.getLocationFromAddress(this, mUser.address), MapsUtil.ZOOM_MAX);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        Snackbar.make(mPosition, getResources().getQuantityString(R.plurals.carPassesAvailable, cont, cont), Snackbar.LENGTH_LONG).show();

        if (cont != EMPTY) {
            //se vi sono passaggi allora il FAB diventa visibile
            mPosition.setVisibility(View.VISIBLE);
        }
        click++;

    }

    /**
     * Funzione che permette di verificare la concessione dei permessi
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    searchPosition();
                }
        }
    }

    /**
     * Funzione che permette di ottenere una istanza della mappa
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (externalUser != true) {
            if (dati.getBoolean(MapsUtil.TRAVEL)) {
                mMap.clear();

                if (dati.getBoolean(MapsUtil.OFFERT)) { //bundle proveniente da travel activity
                    TravelFragmentMaps mTravelFragmentMaps = TravelFragmentMaps.newInstance(dati);
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, mTravelFragmentMaps, MapsUtil.OFFERT).commit();

                    mTravel = dati.getParcelable("travel");
                    HashMap<String, Object> mHashMapTravel = new HashMap<>();
                    mHashMapTravel.put("travelId", mTravel.id);
                    mTravels = ApiUtil.getTravels(mHashMapTravel);

                    HashMap<String, Object> mHashMap = new HashMap<>();
                    mHashMap.put("travelId", mTravel.id);
                    TravelRequest[] travelRequests = ApiUtil.getTravelRequests(mHashMap);

                    mPosition.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_purple)));
                    mPosition.setImageResource(R.drawable.ic_question_white);
                    mPosition.setVisibility(View.VISIBLE);
                    mPosition.setOnClickListener(onCLickToOffert);

                    for (TravelRequest t : travelRequests) {
                        User u = ApiUtil.getUser(t.userId);
                        if (u.id != mUser.id)
                            MapsUtil.addMarkerToOffert(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), u.address), getLayoutInflater(), u);
                    }

                    Company mCompany = ApiUtil.getCompany(mUser.companyId);
                    MapsUtil.addMarkerPositionStandard(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), mUser.address), getLayoutInflater(), "Casa - " + mUser.address);
                    MapsUtil.addMarkerPositionStandard(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), mCompany.address), getLayoutInflater(), "Azienda -" + mCompany.address);

                    CameraPosition cameraPosition = MapsUtil.setCamera(MapsUtil.getLocationFromAddress(this, mUser.address), MapsUtil.ZOOM_MIN);
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    //controllo sul giorno e sulla presenza di richieste in sospeso
                    //per poter iniziare il tracking
                    if (MapsUtil.validateDate(DateUtil.formatToIta(mTravel.date)) && !MapsUtil.controlPendingRequest(mUser, travelRequests)) {
                        mPosition.setImageResource(R.drawable.ic_track);
                        mPosition.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_purple)));

                        if (mUser.id == User.USER_DEMO || mUser.id == User.MOBILITY_DEMO)
                            mPosition.setOnClickListener(auth);
                        else
                            mPosition.setOnClickListener(trak);
                    }

                } else if (!dati.getBoolean(MapsUtil.OFFERT)) {
                    TravelFragmentMaps mTravelFragmentMaps = TravelFragmentMaps.newInstance(dati);
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, mTravelFragmentMaps, MapsUtil.REQUIRED).commit();

                    mPosition.setVisibility(View.GONE);

                    mTravel = dati.getParcelable("travel");
                    User u = ApiUtil.getUser(mTravel.ownerId);

                    MapsUtil.addMarkerToRequest(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), u.address), getLayoutInflater(), u);
                    Company mCompany = ApiUtil.getCompany(mUser.companyId);
                    MapsUtil.addMarkerPositionStandard(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), mUser.address), getLayoutInflater(), "Casa - " + mUser.address);
                    MapsUtil.addMarkerPositionStandard(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), mCompany.address), getLayoutInflater(), "Azienda -" + mCompany.address);

                    CameraPosition cameraPosition = MapsUtil.setCamera(MapsUtil.getLocationFromAddress(this, mUser.address), MapsUtil.ZOOM_MIN);
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    //controllo sul giorno per poter dar via al tracking
                    if (MapsUtil.validateDate(DateUtil.formatToIta(mTravel.date))) {
                        HashMap<String, Object> mHashMap = new HashMap<>();
                        mHashMap.put("travelId", mTravel.id);
                        mHashMap.put("userId", mUser.id);
                        TravelRequest[] travelRequests = ApiUtil.getTravelRequests(mHashMap);
                        if (travelRequests[0].status == TravelRequest.ACCEPTED_REQUEST) {
                            mPosition.setImageResource(R.drawable.ic_track);
                            mPosition.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_purple)));
                            mPosition.setVisibility(View.VISIBLE);
                            if (mUser.id == User.USER_DEMO || mUser.id == User.MOBILITY_DEMO)
                                mPosition.setOnClickListener(auth);
                            else
                                mPosition.setOnClickListener(trak);
                        }
                    }
                }

            } else {
                CameraPosition cameraPosition = MapsUtil.setCamera(MapsUtil.getLocationFromAddress(this, mUser.address), MapsUtil.ZOOM_MAX);
                MapsUtil.addMarkerPositionStandard(getContext(), mMap, MapsUtil.getLocationFromAddress(getContext(), mUser.address), getLayoutInflater(), mUser.address);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }


    /**
     * Funzione che permette di aggiungere la posizione
     * dell'utente corrente alla mappa
     * Funzione attivabile solo nel caso di
     * visitatori o utenti non ancora verificati dall'azienda
     */
    private void searchPosition() {
        try {
            mPosition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MapsUtil.isOnline(getContext())) {
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
                        mtoolbar.setSubtitle(getResources().getString(R.string.searching));
                    } else {
                        Snackbar.make(mPosition, getResources().getString(R.string.offline), Snackbar.LENGTH_SHORT)
                                .setAction(getResources().getString(R.string.goToSetting), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                                    }
                                }).show();
                    }
                }
            });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * Thread per creazione e set toolbar
     */
    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar = (Toolbar) findViewById(R.id.my_toolbar);
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setSubtitle(getResources().getString(R.string.subtitle));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    /**
     * Thread per la creazione del navigation Drawer
     */

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            //controllo se l'utente corrente è un visitatore o meno
            if (externalUser == true) {
                //set visibilità del campo inerente credenziali user e
                //item nav tranne registarzione
                mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT).setVisibility(View.GONE);
                for (int i = 0; i < mMenu.size(); i++)
                    if (mMenu.getItem(i).getItemId() == R.id.registrazione ||
                            mMenu.getItem(i).getItemId() == R.id.userDEMO ||
                            mMenu.getItem(i).getItemId() == R.id.mobilityManageDEMO)
                        mMenu.getItem(i).setVisible(true);
                    else
                        mMenu.getItem(i).setVisible(false);
            } else {
                if (mUser.id == User.MOBILITY_DEMO || mUser.id == User.USER_DEMO) {
                    mMenu.findItem(R.id.LogOut).setVisible(false);
                    mMenu.findItem(R.id.registrazione).setVisible(true);
                }

                //se l'utente non è stato ancora verifato vengono mostrati solo
                //gli item inerenti Gestione Account, LogOut e Mappa
                if (mUser.companyVerified != User.COMPANY_VERIFIED) {
                    for (int i = 0; i < mMenu.size(); i++)
                        if (!(mMenu.getItem(i).getItemId() == R.id.Home || mMenu.getItem(i).getItemId() == R.id.Account || mMenu.getItem(i).getItemId() == R.id.LogOut))
                            mMenu.getItem(i).setVisible(false);
                }
                //se l'utente è un mobility manager gli saranno mostrati
                //tutti gli item presenti nel menu ad eccezione dell'item
                //registrazione
                if (mUser.role == User.MOBILITY_MANAGER) {
                    mMenu.findItem(R.id.mobilityManage).setVisible(true);
                    mMenu.findItem(R.id.mobilityList).setVisible(true);
                }
            }

            //Click inerente il nav
            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));
                            finish();
                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));
                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));
                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));
                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));
                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.userDEMO:
                            startActivity(new Intent(getContext(), AuthActivity.class).putExtra("id", User.USER_DEMO));
                            break;
                        case R.id.mobilityManageDEMO:
                            startActivity(new Intent(getContext(), AuthActivity.class).putExtra("id", User.MOBILITY_DEMO));
                            break;
                    }

                    return true;
                }
            });
        }
    });

    /**
     * Funzione che permette di fare l'inflate per creare la gui
     * del menu della toolbar
     *
     * @param menu
     * @return
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (externalUser != true && mUser.companyVerified == User.COMPANY_VERIFIED)
            getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        else
            super.onCreateOptionsMenu(menu);
        return true;
    }

    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.search:
                FragmentManager fragmentManager = getSupportFragmentManager();
                //controllo fasce orarie
                LinkedHashMap<String, Integer> mListWorkingHours = ApiUtil.getWorkingHours(mUser.companyId);
                if (mListWorkingHours.isEmpty()) {
                    Snackbar.make(mPosition, getContext().getResources().getString(R.string.addWorkingHours), Snackbar.LENGTH_SHORT).show();
                    break;
                }
                if (click % 2 == 0) {//permette di rimuovere il fragment con click sul search button
                    Bundle arg = new Bundle();
                    arg.putInt("azienda", mUser.companyId);
                    mSearchFragment = SearchFragment.newInstance(arg);
                    fragmentManager.beginTransaction().replace(R.id.frameLayout, mSearchFragment, MapsUtil.SEARCH).commit();
                    click++;
                    break;
                } else {
                    fragmentManager.beginTransaction().remove(fragmentManager.findFragmentByTag(MapsUtil.SEARCH)).commit();
                    click++;
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Funzione che permette di ottenere un riferimento del
     * contesto corrente
     *
     * @return
     */

    private Context getContext() {
        return this;
    }

    /**
     * Funzione per la gestione della pressione del tasto
     * back
     */

    @Override
    public void onBackPressed() {
        Boolean flag = false;
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> list = fragmentManager.getFragments();
        if (list.size() > 1)
            mPosition.setVisibility(View.GONE);
        for (Fragment f : list) {
            if (!f.toString().contains(MapsUtil.FRAGMAPS)) {
                fragmentManager.beginTransaction().remove(f).commit();
                flag = true;
            }
        }
        if (flag == false)
            if (mLocationManager != null)
                mLocationManager.removeUpdates(mLocationListener);
        super.onBackPressed();

    }

    /**
     * Listener per ricavare la posizione dell'utente tramite
     * network provider
     */

    public LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            DateTime now = new DateTime();
            try {
                mMap.clear();

                LatLng userLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                MapsUtil.addMarkerPositionStandard(getContext(), mMap, userLatLng, getLayoutInflater(), MapsUtil.getAddressFromLocation(getContext(), userLatLng));
                CameraPosition cameraPosition = MapsUtil.setCamera(userLatLng, MapsUtil.ZOOM_MAX);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                mtoolbar.setSubtitle(getResources().getString(R.string.subtitle));

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("IOException", e.getMessage());
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    };


    /**
     * intent per la visualizzazione delle richieste
     */

    public View.OnClickListener onClickToRequest = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            intent = new Intent(MapsActivity.this, UserTravelOffertRequestActivity.class);
            intent.putParcelableArrayListExtra(MapsUtil.REQUIRED, fromArrayToArrayList(mTravels));
            startActivity(intent);
        }
    };

    /**
     * intent per la visualizzazione delle offerte
     */
    public View.OnClickListener onCLickToOffert = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            intent = new Intent(MapsActivity.this, UserTravelOffertRequestActivity.class);
            intent.putParcelableArrayListExtra(MapsUtil.OFFERT, fromArrayToArrayList(mTravels));
            startActivity(intent);
        }
    };


    /**
     * funzione che permette di trasformare un array in un arraylist
     * di Travel
     *
     * @param travels
     * @return
     */

    private ArrayList<Travel> fromArrayToArrayList(Travel[] travels) {
        ArrayList<Travel> mArrayList = new ArrayList<>();
        for (Travel t : travels) {
            mArrayList.add(t);
        }
        return mArrayList;
    }

    /**
     * gestione del risultato derivante dall'activityforresult del tracking
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {
            case TrackingUtil.LOCATION_PERMISSIONS_REJECTED:
                Snackbar.make(mPosition, getResources().getString(R.string.LOCATION_PERMISSIONS_REJECTED), Snackbar.LENGTH_LONG).show();
                break;
            case TrackingUtil.TRACKING_NOT_STARTED:
                Snackbar.make(mPosition, getResources().getString(R.string.TRACKING_NOT_STARTED), Snackbar.LENGTH_LONG).show();
                break;
            case TrackingUtil.GPS_NOT_ENABLED:
                Snackbar.make(mPosition, getResources().getString(R.string.GPS_NOT_ENABLED), Snackbar.LENGTH_LONG).show();
                break;
            case TrackingUtil.TOO_FAR_ERROR:
                Snackbar.make(mPosition, getResources().getString(R.string.TOO_FAR_ERROR), Snackbar.LENGTH_LONG).show();
                break;
        }
    }

    public View.OnClickListener trak = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(MapsActivity.this, TrackingActivity.class).putExtra("travel", mTravel), 1);
        }
    };

    public View.OnClickListener auth = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AuthManager.logout((Activity) getContext());
        }
    };
}