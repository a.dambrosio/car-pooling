package it.uniba.di.sms.max2d.carpooling;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.Api;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;
import static java.security.AccessController.getContext;


public class TraverlFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    //private View myview;
    private RecyclerView list;
    private View mMainViewRichiesti;
    private RecyclerView recyclerViewRichiesti;
    private View mMainViewOfferti;
    private RecyclerView recyclerViewOfferti;
    private List<User> userListRichiesti = new ArrayList<>();
    private List<User> userListOfferti = new ArrayList<>();
    private User currentUser;



    public TraverlFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static TraverlFragment newInstance(int sectionNumber) {
        TraverlFragment fragment = new TraverlFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public static TraverlFragment newInstance(Bundle arg) {
        TraverlFragment fragment = new TraverlFragment();
        fragment.setArguments(arg);
        return fragment;
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            currentUser = ApiUtil.getUser(user.getUid());


            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {

                mMainViewOfferti = inflater.inflate(R.layout.activity_users, container, false);
                recyclerViewOfferti = (RecyclerView) mMainViewOfferti.findViewById(R.id.user_list);
                recyclerViewOfferti.setHasFixedSize(true);

                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerViewOfferti.setLayoutManager(layoutManager);

                ((TravelActivity)getActivity()).showProgressDialog();

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Map<String, Object> filters = new HashMap<>();
                        filters.put("ownerId", currentUser.id);
                        filters.put("notDeleted", true);
                        Travel[] travels = ApiUtil.getTravels(filters);
                        final AdapterOfferti mAdapter = new AdapterOfferti(travels, getContext());

                        try {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerViewOfferti.setAdapter(mAdapter);

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

                thread.start();



                // mAdapter.notifyDataSetChanged();


                return mMainViewOfferti;

            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {


                mMainViewRichiesti = inflater.inflate(R.layout.activity_users, container, false);

                recyclerViewRichiesti = (RecyclerView) mMainViewRichiesti.findViewById(R.id.user_list);
                recyclerViewRichiesti.setHasFixedSize(true);


                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerViewRichiesti.setLayoutManager(layoutManager);

                ((TravelActivity)getActivity()).showProgressDialog();

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Map<String, Object> filters = new HashMap<>();
                        filters.put("userId", currentUser.id);
                        filters.put("notOwnerId", currentUser.id);
                        filters.put("notDeleted", true);
                        TravelRequest[] travelRequests = ApiUtil.getTravelRequests(filters);
                        try {
                            final AdapterRichiesti mAdapter = new AdapterRichiesti(travelRequests, getActivity());

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerViewRichiesti.setAdapter(mAdapter);
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();


                return mMainViewRichiesti;

            }
        }
        return null;

    }
}