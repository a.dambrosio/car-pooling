package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AdapterUserTravelRichiesti extends RecyclerView.Adapter<AdapterUserTravelRichiesti.UserViewHolder> {
    User[] users;
    Context mContext;
    LayoutInflater inflater;
    Travel[] travels;
    View mView;
    static final int ONE_SECOND_AND_HALF = 1500;
    public AdapterUserTravelRichiesti(User[] users, Travel[] travel, Context mContext) {
        this.travels = travel;
        this.users = users;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }


    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV;
        ImageView avatarImage;
        CardView cv;
        TextView requestText;
        TextView cellular;
        TextView type;
        TextView date;


        public UserViewHolder(View view) {
            super(view);
            nameTV = (TextView) view.findViewById(R.id.ProfileNameTravelR);
            cv = (CardView) view.findViewById(R.id.card_viewUserTravelRequired);
            avatarImage = (ImageView) view.findViewById(R.id.ProfileAvatarImageTravelR);
            requestText = (TextView) view.findViewById(R.id.stateRequestR);
            type = (TextView) view.findViewById(R.id.typeViaggioR);
            cellular = (TextView) view.findViewById(R.id.CellularViaggioR);
            date = (TextView) view.findViewById(R.id.dateViaggioR);

        }


    }

    @Override
    public AdapterUserTravelRichiesti.UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_travel_required, parent, false);
        mView = itemView;
        return new AdapterUserTravelRichiesti.UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AdapterUserTravelRichiesti.UserViewHolder holder, final int position) {
        try {
            FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            final User currentUser = ApiUtil.getUser(mFirebaseUser.getUid());

            final User user = users[position];
            final Travel travel = travels[position];
            Picasso avatarLoader = Picasso.get();
            final String name = user.name + " " + user.surname;

            holder.nameTV.setText(name);

            holder.nameTV.setTextColor(mContext.getResources().getColor(android.R.color.black));
            holder.cellular.setText(user.phone);
            holder.date.setText(DateUtil.formatToIta(travel.date));

            if (travel.type == Travel.HOME_WORK)
                holder.type.setText(mContext.getResources().getString(R.string.home_office));
            else
                holder.type.setText(mContext.getResources().getString(R.string.office_home));
            HashMap<String, Object> mHashMap = new HashMap<>();
            mHashMap.put("userId", currentUser.id);
            mHashMap.put("travelId", travel.id);
            TravelRequest[] travelRequest = ApiUtil.getTravelRequests(mHashMap);

            switch (travelRequest.length) {
                case 0:
                    holder.requestText.setText(mContext.getString(R.string.request_not_done));
                    break;
                case 1:
                    holder.requestText.setText(mContext.getString(R.string.request_already_done));
                    break;
            }

            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!holder.requestText.getText().toString().equals(mContext.getString(R.string.request_already_done))) {
                        TravelRequest travelRequest = new TravelRequest(-1, currentUser.id, travel.id, 0, TravelRequest.PENDING_REQUEST);
                        Boolean vCC = ApiUtil.storeTravelRequest(travelRequest);
                        Log.d("BB", String.valueOf(vCC));
                        reload();
                    } else {
                        mContext.startActivity(new Intent(mContext, TravelActivity.class));
                    }
                }
            });


            if (user.avatarUrl.isEmpty()) {
                avatarLoader.load(R.drawable.default_image).into(holder.avatarImage);
            } else {
                //avatarLoader.load(avatarUrl).into(holder.avatarImage);
                avatarLoader.load(user.avatarUrl)
                        .into(holder.avatarImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Bitmap imageBitmap = ((BitmapDrawable) holder.avatarImage.getDrawable()).getBitmap();
                                RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                                imageDrawable.setCircular(true);
                                imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                                holder.avatarImage.setImageDrawable(imageDrawable);
                            }

                            @Override
                            public void onError(Exception $e) {
                                holder.avatarImage.setImageResource(R.drawable.default_image);
                            }
                        });
            }


        } catch (Exception e) {
            reload();
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return users.length;
    }

    private void reload() {
        ((Activity) mContext).finish();
        ((Activity) mContext).overridePendingTransition(0, 0);
        ((Activity) mContext).startActivity(((Activity) mContext).getIntent());
        ((Activity) mContext).overridePendingTransition(0, 0);

    }
}
