package it.uniba.di.sms.max2d.carpooling;


import org.json.JSONObject;

public class TravelRequest {

    public final static int PENDING_REQUEST = 0;
    public final static int ACCEPTED_REQUEST = 1;
    public final static int REJECTED_REQUEST = 2;

    public int id;
    public int userId;
    public int travelId;
    public int score;
    public int status;

    public TravelRequest(int id, int userId, int travelId, int score, int status) {
        this.id = id;
        this.userId = userId;
        this.travelId = travelId;
        this.score = score;
        this.status = status;
    }

    public TravelRequest(JSONObject request) {
        try {
            this.id = request.getInt("idUT");
            this.userId = request.getInt("FKUserUT");
            this.travelId = request.getInt("FKTravelUT");
            this.score = request.getInt("scoreUT");
            this.status = request.getInt("statusUT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJsonObject() {
        try {
            JSONObject tr = new JSONObject();
            tr.put("FKUserUT", this.userId);
            tr.put("FKTravelUT", this.travelId);
            tr.put("scoreUT", this.score);
            tr.put("statusUT", this.status);
            return tr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



}
