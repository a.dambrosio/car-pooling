package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.app.DatePickerDialog;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;


import static it.uniba.di.sms.max2d.carpooling.ApiUtil.storeTravel;
import static it.uniba.di.sms.max2d.carpooling.Travel.*;


public class InsertTravel extends AppCompatActivity {

    final int WEEKLY_TRAVEL_NOT_CHECKED = 0;
    final int WEEKLY_TRAVEL_CHECKED = 1;
    private final int RESULT_NO_WORKING_HOUR =4;
    private final int RESULT_ANNULL =10;
    private final int RESULT_INSERT_ERROR = 17;
    String MESSAGGE_ERROR_FIELD;
    String MESSAGGE_ERROR_SEATS;
    int currentDay;
    int currentMonth;
    int currentYear;

    Toolbar mtoolbar;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    User mUser;

    EditText editTextDate;
    Spinner spinnerTimeSlot;
    EditText editTextTypeCar;
    EditText editTextSeatsAvaiable;
    RadioButton radioButtonTravelTypeHO;
    RadioButton radioButtonTravelTypeOH;
    CheckBox checkBoxWeeklyTravel;
    Button buttonInsertTravel;
    TextView textViewRadioGroup;

    TextInputLayout textInputLayoutTypeCar;
    TextInputLayout textInputLayoutData;
    TextInputLayout textInputLayoutSeats;
    TextInputLayout textInputLayoutRadio;



    LinkedHashMap<String, Integer>  workingHourDatabase;

    private FirebaseUser FireBaseUser;
    private User user;


    DatePickerDialog datePickerDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_travel);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mUser = ApiUtil.getUser(currentUser.getUid());


        MESSAGGE_ERROR_FIELD =getResources().getString(R.string.messagge_error_field);
        MESSAGGE_ERROR_SEATS = getResources().getString(R.string.message_error_seats);

        FireBaseUser = FirebaseAuth.getInstance().getCurrentUser();
        user = ApiUtil.getUser(FireBaseUser.getUid());


        workingHourDatabase = ApiUtil.getWorkingHours(user.companyId);
        checkInsertValid();

        editTextDate = (EditText) findViewById(R.id.editTextData);

        editTextTypeCar = (EditText) findViewById(R.id.editTextTypeCar);
        editTextSeatsAvaiable = (EditText) findViewById(R.id.editTextSeatsAvaiable);
        radioButtonTravelTypeHO = (RadioButton) findViewById(R.id.radioButtonHO);
        radioButtonTravelTypeOH = (RadioButton) findViewById(R.id.radioButtonOH);
        spinnerTimeSlot = (Spinner) findViewById(R.id.spinnerTimeSlot);
        checkBoxWeeklyTravel = (CheckBox) findViewById(R.id.checkBoxWeeklyTravel);
        textViewRadioGroup = (TextView) findViewById(R.id.textViewTravelType);

        textInputLayoutTypeCar = (TextInputLayout) findViewById(R.id.inputLayoutTypeCarInsert);
        textInputLayoutData = (TextInputLayout) findViewById(R.id.inputLayoutDataInsert);
        textInputLayoutSeats = (TextInputLayout) findViewById(R.id.inputLayoutSeatsInsert);
        textInputLayoutRadio = (TextInputLayout) findViewById(R.id.inputLayoutRadio);


        buttonInsertTravel = (Button) findViewById(R.id.buttonInsertTravel);
        buttonInsertTravel.setOnClickListener(insertTravel);

        final Calendar c = Calendar.getInstance();
        currentYear = c.get(Calendar.YEAR); // current year
        currentMonth = c.get(Calendar.MONTH) +1; // current month
        currentDay = c.get(Calendar.DAY_OF_MONTH); // current day



        manageDatePicker();

        manageWorkingHourSpinner();

        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);

        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = mUser.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(mUser.name + " " + mUser.surname);

        super.onStart();
    }

    /*
    Inserisci un nuovo passaggio nel database cliccando sul punsante
     */
    public View.OnClickListener insertTravel = new View.OnClickListener() {

        String date;
        int timeSlot;
        String typeCar;
        int seats;
        int travelType;
        int weeklyTravel;


        @Override
        public void onClick(View v) {
            boolean valid = validate();

            if (valid) {
                date = getDate();
                timeSlot = getTimeSlot();
                typeCar = getTypeCar();
                seats = getSeats();
                travelType = getTravelType();
                weeklyTravel = getWeeklyTravel();


                Travel newTravel = new Travel(-1,date.trim(),timeSlot,seats,typeCar.trim(),travelType,weeklyTravel,user.id);


                boolean ok = ApiUtil.storeTravel(newTravel);
                Intent goToMain = getIntent();

                if (ok) {
                    setResult(RESULT_OK, goToMain);
                } else {
                    setResult(RESULT_INSERT_ERROR, goToMain);
                }

                finish();

            }
        }
    };

    private void checkInsertValid(){
        if(workingHourDatabase.isEmpty()){

            Intent goToMain = getIntent();
            setResult(RESULT_NO_WORKING_HOUR,goToMain);
            finish();


        }
    }



    /*
    Restituisce la data indicata dall'utente
     */
    private String getDate() {
        return editTextDate.getText().toString();
    }

    /*
    Restituisce il modello dell'auto indicata dall'utente
     */
    private String getTypeCar() {
        return editTextTypeCar.getText().toString();
    }

    /*
    Restituisce il numero di posto disponibili indicati dall'utente
     */
    private int getSeats() {

        return Integer.parseInt(editTextSeatsAvaiable.getText().toString());
    }

    /*
    Restituisce il tipo di viaggio indicato dall'utente
     */
    private int getTravelType() {
        if (radioButtonTravelTypeHO.isChecked())
                    /*
                    è stato selezionato il tipo di viaggio Casa-ufficio
                     */
            return HOME_WORK;
        else
                     /*
                    è stato selezionato il tipo di viaggio ufficio-casa
                     */
            return WORK_HOME;
    }

    /*
    Restituisce l'indicazione dell'utente riguardante la ripetitività settimanale del viaggio
    Restituisce una stringa se il viaggio è settimanale
    Altrimenti null
     */
    private int getWeeklyTravel() {
        if (checkBoxWeeklyTravel.isChecked())
                    /*
                    Il viaggio viene effettuato settimanalmente
                     */
            return WEEKLY_TRAVEL_CHECKED;
        else
                    /*
                    Il viaggio non viene effettuato settimanalmente
                     */
            return WEEKLY_TRAVEL_NOT_CHECKED;
    }

    /*
    Restituisce l'id della fascia oraria indicata dall'utente, presente nel database
     */
    private int getTimeSlot() {
        String workingHourSelected = spinnerTimeSlot.getSelectedItem().toString();


        int id = workingHourDatabase.get(workingHourSelected);


        return  id;

    }

    /*
      Gestisci il datePicker nella casella data
    */
    private void manageDatePicker() {
        editTextDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if (isFocus == false) {
                    if (editTextDate.length() > 0)
                        textInputLayoutData.setError(null);
                    else
                        textInputLayoutData.setError(MESSAGGE_ERROR_FIELD);
                }
                if (isFocus == true) {
                    // calender class's instance and get current date , month and year from calender
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day


                    datePickerDialog = new DatePickerDialog(InsertTravel.this, new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            // set day of month , month and year value in the edit text
                            editTextDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                    SignInActivity.closeKeyboard(InsertTravel.this, editTextDate.getWindowToken());
                }
            }

        });


        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                datePickerDialog = new DatePickerDialog(InsertTravel.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        editTextDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year));
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
                SignInActivity.closeKeyboard(InsertTravel.this, editTextDate.getWindowToken());
            }
        });

    }

    private void manageWorkingHourSpinner() {

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.list_item, workingHourDatabase.keySet().toArray(new String[workingHourDatabase.size()]));

        spinnerTimeSlot.setAdapter(adapter);

    }


    /*
    Controlla che i campi obbligatori siano inseriti prima che la registrazione venga effettuata
     */
    private boolean validate() {
        Boolean correctInsert = true;

        if (getDate().length() <= 0) {
            correctInsert = false;
            textInputLayoutData.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getDate().length() >0){
            final Calendar c = Calendar.getInstance();

            String insertData[] = getDate().split("/");

            int insertDay = Integer.parseInt(insertData[0]);
            int insertMonth = Integer.parseInt(insertData[1]);
            int insertYear = Integer.parseInt(insertData[2]);

            if(currentYear > insertYear){
                correctInsert = false;
            }else if (currentYear == insertYear && currentMonth > insertMonth){
                correctInsert = false;
            }else if (currentYear == insertYear && currentMonth == insertMonth && currentDay > insertDay )
                correctInsert = false;

            if(!correctInsert)
                textInputLayoutData.setError(getApplicationContext().getString(R.string.error_next_date));



        }

        if (getTypeCar().length() == 0) {

            correctInsert = false;
           textInputLayoutTypeCar.setError(MESSAGGE_ERROR_FIELD);
           

        }

        if (editTextSeatsAvaiable.getText().toString().length() == 0) {
            correctInsert = false;
            textInputLayoutSeats.setError(MESSAGGE_ERROR_FIELD);
        }


        if(editTextSeatsAvaiable.getText().toString().length() != 0){
            if(getSeats() <1){
                correctInsert = false;
                textInputLayoutSeats.setError(getApplicationContext().getString(R.string.error_seats));
            }
        }

        if (!radioButtonTravelTypeHO.isChecked() && !radioButtonTravelTypeOH.isChecked()) {
            correctInsert = false;
            textInputLayoutRadio.setError(MESSAGGE_ERROR_FIELD);
        } else
            textViewRadioGroup.setError(null);
        return correctInsert;
    }

    public void onBackPressed() {
        Intent goBack = new Intent();
        setResult(RESULT_ANNULL, goBack);
        finish();

    }

    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);

                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mtoolbar.setSubtitle(getResources().getString(R.string.insertTravel));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });



    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);

            Menu mMenu = mNavigationView.getMenu();

            if (mUser.id == User.MOBILITY_DEMO || mUser.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if (mUser.role == User.MOBILITY_MANAGER) {

                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));

                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }
                    return true;
                }
            });
        }
    });


    private Context getContext() {
        return this;
    }

}
