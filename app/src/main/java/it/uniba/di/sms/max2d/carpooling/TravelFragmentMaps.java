package it.uniba.di.sms.max2d.carpooling;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class TravelFragmentMaps extends Fragment {

    Context mContext;

    private View mView;

    public static TravelFragmentMaps newInstance(Bundle arg) {
        TravelFragmentMaps fragment = new TravelFragmentMaps();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle mBundle = getArguments();
        Travel mTravel = mBundle.getParcelable("travel");

        if (!mBundle.getBoolean(MapsUtil.OFFERT)) {
            mView = inflater.inflate(R.layout.fragment_required_travel_maps, container, false);

            User user = ApiUtil.getUser(mTravel.ownerId);
            final ImageView mImageView = mView.findViewById(R.id.user_single_image);
            TextView viaggR = mView.findViewById(R.id.viagg);
            TextView cellulareR = mView.findViewById(R.id.cellulare);
            TextView statR = mView.findViewById(R.id.stat);
            TextView carR = mView.findViewById(R.id.car);
            TextView dataR = mView.findViewById(R.id.date);
            mView.findViewById(R.id.cv).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getContext(),TravelActivity.class));
                }
            });

            String date = DateUtil.formatToIta(mTravel.date);
            dataR.setText(Html.fromHtml(" " + date));
            if (mTravel.type == Travel.HOME_WORK)
                viaggR.setText(Html.fromHtml(" " + getApplicationContext().getString(R.string.casalavoro)));
            else
                viaggR.setText(Html.fromHtml(" " + getApplicationContext().getString(R.string.lavorocasa)));


            cellulareR.setText(Html.fromHtml(" " + user.phone));

            String status = mBundle.getString("STATUS");

            statR.setText(Html.fromHtml(" " + status));


            carR.setText(Html.fromHtml(" " + mTravel.car));
            String avatarUrl = "http://";
            if (!user.avatarUrl.isEmpty())
                avatarUrl = user.avatarUrl;

            Picasso avatarLoader = Picasso.get();

            avatarLoader.load(avatarUrl)
                    .into(mImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            mImageView.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            mImageView.setImageResource(R.drawable.default_image);
                        }
                    });


        } else {

            mView = inflater.inflate(R.layout.offerti, container, false);

            Travel travel = mBundle.getParcelable("travel");

            WorkingHour wh = ApiUtil.getWorkingHour(travel.workingHourId);
            String hour = "";
            String travelType;


            if (travel.type == Travel.HOME_WORK) {
                travelType = "casa->lavoro";
                hour = wh.start;

            } else {
                travelType = "lavoro->casa";
                hour = wh.end;
            }

            Map<String, Object> filters = new HashMap<>();
            filters.put("travelId", travel.id);
            TravelRequest[] travelRequests = ApiUtil.getTravelRequests(filters);

            int takenSeats = 0;

            int waitingReqNum = 0;

            for (TravelRequest travelRequest : travelRequests) {
                if (travelRequest.status == TravelRequest.PENDING_REQUEST) {
                    waitingReqNum++;
                } else if (travelRequest.status == TravelRequest.ACCEPTED_REQUEST) {
                    takenSeats++;
                }
            }

            TextView Viaggio = mView.findViewById(R.id.viaggOfferti);
            TextView data = mView.findViewById(R.id.dateOfferti);
            TextView stato = mView.findViewById(R.id.statOfferti);
            TextView posti = mView.findViewById(R.id.postiOccupatiOfferti);

            data.setText(Html.fromHtml(" " + travel.date + " - " + hour));
            Viaggio.setText(Html.fromHtml( " " + travelType));
            stato.setText(Html.fromHtml(" " + waitingReqNum +" "+ getApplicationContext().getString(R.string.pending_requests)));
            posti.setText(Html.fromHtml( " " + takenSeats + " "+ getApplicationContext().getString(R.string.su)+ " " + travel.seats + " " + getApplicationContext().getString(R.string.postiOccupati)));
            ImageView img = (ImageView) mView.findViewById(R.id.user_single_imageOfferti);

            if (waitingReqNum > 0) {
                img.setImageResource(R.drawable.ic_directions_car_orange_24dp);
            }

        }


        return mView;
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        super.onAttach(context);
    }
}