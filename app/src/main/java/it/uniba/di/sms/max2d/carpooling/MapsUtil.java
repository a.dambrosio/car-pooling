package it.uniba.di.sms.max2d.carpooling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.model.DirectionsResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class MapsUtil {
    public static final String TRAVEL = "tra";
    public static final String OFFERT = "offert";
    public static final String SEARCH = "search";
    public static final String REQUIRED = "required";
    public static final String FRAGMAPS = "SupportMapFragment";


    public static final int ZOOM_MAX = 17;
    public static final int ZOOM_MIN = 12;


    /**
     * controlla se vi sono richeiste in sospeso
     * @param mUser
     * @param travelRequests
     * @return
     */
    public static Boolean controlPendingRequest(User mUser,TravelRequest [] travelRequests){
        Boolean control = false;
        for(TravelRequest t : travelRequests){
            User u = ApiUtil.getUser(t.userId);
            if(u.id!=mUser.id){
                if(t.status==TravelRequest.PENDING_REQUEST){
                    control = true;
                }
            }
        }
        return control;
    }

    /**
     * funzione che permette di verificare che la distanza tra due coordinate
     * sia minore di 1km
     * @param latStart
     * @param lonStart
     * @param latEnd
     * @param lonEnd
     * @return
     */
    public static Boolean distanceTo(double latStart,double lonStart, double latEnd,double lonEnd){
        final float METRI = 1000;
        Location start = new Location("");
        start.setLatitude(latStart);
        start.setLongitude(lonStart);

        Location end = new Location("");
        end.setLatitude(latEnd);
        end.setLongitude(lonEnd);

        return start.distanceTo(end) <= METRI;
    }

    /**
     * funzione di utilità per il controllo dello stato OnLine|OffLine
     * dell'utente
     * @param c
     * @return
     */
    public static boolean isOnline(Context c) {

        ConnectivityManager cm =
                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    /**
     * Costruisce una istanza di camera position che permette di settare
     * l'inquadratura della mappa
     * @param position
     * @param zoom
     * @return
     */
    public static CameraPosition setCamera(LatLng position, int zoom) {
        return new CameraPosition.Builder()
                .target(position)
                .zoom(zoom)
                .build();
    }

    /**
     * Rimuove il fragment avente come tag la stringa s
     *
     * @param mActivity
     * @param s Tag utilizzato nel replace del fragment
     */

    public static void removeFragment(AppCompatActivity mActivity, String s) {
        Fragment mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(s);
        mActivity.getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
    }

    /**
     * Fun per trasformare un vector in bitmap
     *
     * @param context
     * @param drawableId
     * @return
     */
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     * Funzione che aggiunge i marker alla mappa
     *
     * @param results
     * @param mMap
     */
    public static void addMarkersToMap(DirectionsResult results, GoogleMap mMap, @Nullable HashMap<LatLng, Boolean> marker) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[0].legs[0].startLocation.lat, results.routes[0].
                legs[0].startLocation.lng)).title(results.routes[0].legs[0].startAddress));
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[0].legs[0].endLocation.lat, results.routes[0].
                legs[0].endLocation.lng)).title(results.routes[0].legs[0].endAddress).snippet(getEndLocationTitle(results)));
    }

    /**
     * Funzione che prendendo una oggetto DirectionResult restituisce una stringa contenente i dati
     * inerenti alla distanza e al tempo stimato per comprire la distanza tra due marker
     *
     * @param results
     * @return String
     */
    private static String getEndLocationTitle(DirectionsResult results) {
        return "Time :" + results.routes[0].legs[0].duration.humanReadable +
                " Distance :" + results.routes[0].legs[0].distance.humanReadable;
    }

    /**
     * aggiunge un marker alla mappa di tipo standard
     * @param mContext
     * @param mMap
     * @param mLatLng
     * @param mLayoutInflater
     * @param userAddress
     */
    public static void addMarkerPositionStandard(Context mContext, GoogleMap mMap, LatLng mLatLng, LayoutInflater mLayoutInflater, String userAddress) {
        Bitmap icon = MapsUtil.getBitmapFromVectorDrawable(mContext, R.drawable.ic_full_circle_red);
        BitmapDescriptor iconDesc = BitmapDescriptorFactory.fromBitmap(icon);
        MarkerOptions mMarkerOptions = new MarkerOptions().position(mLatLng).icon(iconDesc);
        mMarkerOptions.title(userAddress);
        mMap.addMarker(mMarkerOptions).showInfoWindow();
    }

    /**
     * aggiunga un marker di tipo Richiesta alla mappa
     * @param mContext
     * @param mMap
     * @param mLatLng
     * @param mLayoutInflater
     * @param mUser
     */
    public static void addMarkerToRequest(Context mContext, GoogleMap mMap, LatLng mLatLng, LayoutInflater mLayoutInflater, User mUser) {
        Bitmap icon = MapsUtil.getBitmapFromVectorDrawable(mContext, R.drawable.ic_full_circle);
        BitmapDescriptor iconDesc = BitmapDescriptorFactory.fromBitmap(icon);
        MarkerOptions mMarkerOptions = new MarkerOptions().position(mLatLng).icon(iconDesc);
        mMarkerOptions.title(mUser.name+" "+mUser .surname+" "+mUser.address);
        mMap.addMarker(mMarkerOptions).showInfoWindow();

    }

    /**
     * aggiunga un marker di tipo Offerto alla mappa
     * @param mContext
     * @param mMap
     * @param mLatLng
     * @param mLayoutInflater
     * @param mUser
     */
    public static void addMarkerToOffert(Context mContext, GoogleMap mMap, LatLng mLatLng, LayoutInflater mLayoutInflater, User mUser) {
        Bitmap icon = MapsUtil.getBitmapFromVectorDrawable(mContext, R.drawable.ic_question);
        BitmapDescriptor iconDesc = BitmapDescriptorFactory.fromBitmap(icon);
        MarkerOptions mMarkerOptions = new MarkerOptions().position(mLatLng).icon(iconDesc);
        mMarkerOptions.title(mUser.name+" "+mUser .surname+" "+mUser.address);
        mMap.addMarker(mMarkerOptions).showInfoWindow();

    }


    /**
     * Funzione che calcola e aggiunge alla mappa graficamente la linea poligonale
     * che congiunge 2 marker
     *
     * @param results
     * @param mMap
     */
    private static void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[0].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath));
    }

    /**
     * funzione che esegue il match tra una stringa e la data corrente
     * @param mString
     * @return
     */
    public static boolean validateDate(String mString) {

        final Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR); // current year
        int currentMonth = c.get(Calendar.MONTH) + 1; // current month
        int currentDay = c.get(Calendar.DAY_OF_MONTH); // current day

        String insertData[] = mString.split("/");

        int insertDay = Integer.parseInt(insertData[0]);
        int insertMonth = Integer.parseInt(insertData[1]);
        int insertYear = Integer.parseInt(insertData[2]);

        if (currentYear == insertYear && currentMonth == insertMonth && currentDay == insertDay)
            return true;
        else
            return false;
    }

    /**
     * Funzione che trasforma un indirizzo in coordinate
     *
     * @param context
     * @param strAddress
     * @return
     */
    public static LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return p1;
    }

    /**
     * funzione che trasforma una coordinata geografica in
     * indirizzo
     * @param mContext
     * @param mLatLng
     * @return
     */
    public static String getAddressFromLocation(Context mContext, LatLng mLatLng) {

        Geocoder coder = new Geocoder(mContext);
        StringBuilder mStringBuilder = new StringBuilder();
        try {
            List<Address> listAddress = coder.getFromLocation(mLatLng.latitude, mLatLng.longitude, 5);

            if (listAddress == null)
                return null;

            mStringBuilder.append(listAddress.get(0).getAddressLine(0)); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()


        } catch (IOException e) {
            e.printStackTrace();
        }

        return mStringBuilder.toString();
    }

}
