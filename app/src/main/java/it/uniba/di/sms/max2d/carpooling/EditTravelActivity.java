package it.uniba.di.sms.max2d.carpooling;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.app.DatePickerDialog;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthProvider;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;


import it.uniba.di.sms.max2d.carpooling.ApiUtil;
import it.uniba.di.sms.max2d.carpooling.R;
import it.uniba.di.sms.max2d.carpooling.Travel;
import it.uniba.di.sms.max2d.carpooling.User;

import static android.content.RestrictionsManager.RESULT_ERROR;
import static android.content.RestrictionsManager.RESULT_ERROR_BAD_REQUEST;
import static com.facebook.FacebookSdk.getApplicationContext;
import static it.uniba.di.sms.max2d.carpooling.ApiUtil.storeTravel;
import static it.uniba.di.sms.max2d.carpooling.Travel.*;


public class EditTravelActivity extends AppCompatActivity {

    final int WEEKLY_TRAVEL_NOT_CHECKED = 0;
    final int WEEKLY_TRAVEL_CHECKED = 1;
    final int RESULT_ANNULL =10;
    String MESSAGGE_ERROR_FIELD;
    String MESSAGGE_ERROR_SEATS;
    int currentDay;
    int currentMonth;
    int currentYear;

    

    Toolbar mtoolbar;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    EditText editTextDate;
    Spinner spinnerTimeSlot;
    EditText editTextTypeCar;
    EditText editTextSeatsAvaiable;
    RadioButton radioButtonTravelTypeHO;
    RadioButton radioButtonTravelTypeOH;
    CheckBox checkBoxWeeklyTravel;
    Button buttonEditTravel;
    TextView textViewRadioGroup;
    Switch switchEdit;

    TextInputLayout textInputLayoutEditTypeCar;
    TextInputLayout textInputLayoutEditDate;
    TextInputLayout textInputLayoutEditSeats;


    LinkedHashMap<String, Integer>  workingHourDatabase;

    Travel travel;

    Bundle travelBundle;
    int idTravel;
    HashMap<String,Object> filters;



    private FirebaseUser FireBaseUser;
    private User user;


    DatePickerDialog datePickerDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_travel);


        MESSAGGE_ERROR_FIELD =getResources().getString(R.string.messagge_error_field);
        MESSAGGE_ERROR_SEATS = getResources().getString(R.string.message_error_seats);


        FireBaseUser = FirebaseAuth.getInstance().getCurrentUser();
        user = ApiUtil.getUser(FireBaseUser.getUid());

        /*
        Nasconde la tastiera appena entrati nell'activity
         */
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        travelBundle= getIntent().getExtras();
        idTravel = travelBundle.getInt("idTravel");
        filters = new HashMap<>();

        filters.put("travelId",idTravel);

        /*
        Controlla se il viaggio è stato già richiesto da altri utenti
        In questo caso l'activity restituisce un errore
         */
        verify();


        editTextDate = (EditText) findViewById(R.id.editTextDataEdit);
        spinnerTimeSlot = (Spinner) findViewById(R.id.spinnerTimeSlotEdit);
        editTextTypeCar = (EditText) findViewById(R.id.editTextTypeCarEdit);
        editTextSeatsAvaiable = (EditText) findViewById(R.id.editTextSeatsAvaiableEdit);
        radioButtonTravelTypeHO = (RadioButton) findViewById(R.id.radioButtonHOEdit);
        radioButtonTravelTypeOH = (RadioButton) findViewById(R.id.radioButtonOHEdit);
        checkBoxWeeklyTravel = (CheckBox) findViewById(R.id.checkBoxWeeklyTravelEdit);
        textViewRadioGroup = (TextView) findViewById(R.id.textViewTravelTypeEdit);

        textInputLayoutEditTypeCar = (TextInputLayout) findViewById(R.id.inputLayoutTypeCarEdit);
        textInputLayoutEditDate = (TextInputLayout) findViewById(R.id.inputLayoutDataEdit);
        textInputLayoutEditSeats = (TextInputLayout) findViewById(R.id.inputLayoutSeatsEdit);

        buttonEditTravel = (Button) findViewById(R.id.buttonEditTravel);
        switchEdit =(Switch) findViewById(R.id.switchTravelEdit);



        buttonEditTravel.setOnClickListener(editTravel);




        final Calendar c = Calendar.getInstance();
        currentYear = c.get(Calendar.YEAR); // current year
        currentMonth = c.get(Calendar.MONTH) +1; // current month
        currentDay = c.get(Calendar.DAY_OF_MONTH); // current day

        workingHourDatabase = ApiUtil.getWorkingHours(user.companyId);


        initialVisibility(false);

        enableEdit();

        manageDatePicker();

        manageWorkingHourSpinner();

        loadFromDatabase();

        mtoolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.my_toolbar);


        threadToolbar.start();
        try {
            threadToolbar.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        
    }

    @Override
    protected void onStart() {
        final ImageView profilImage = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPIMAGE);
        TextView profilName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.NAVTOPEDIT);

        String avatarUrl = user.avatarUrl;
        Picasso avatarLoader = Picasso.get();

        if (avatarUrl.isEmpty()) {
            avatarLoader.load(R.drawable.default_image).into(profilImage);
        } else {
            avatarLoader.load(avatarUrl)
                    .into(profilImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) profilImage.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getContext().getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            profilImage.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError(Exception $e) {
                            profilImage.setImageResource(R.drawable.default_image);
                        }
                    });
        }

        profilName.setText(user.name + " " + user.surname);

        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("switchCheked",switchEdit.isChecked());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        boolean isSwitchChecked = savedInstanceState.getBoolean("switchCheked");

        if(isSwitchChecked){
            switchEdit.setChecked(true);
            editTextTypeCar.setEnabled(true);
            spinnerTimeSlot.setEnabled(true);
            editTextDate.setEnabled(true);
            editTextSeatsAvaiable.setEnabled(true);
            radioButtonTravelTypeHO.setEnabled(true);
            radioButtonTravelTypeOH.setEnabled(true);
            checkBoxWeeklyTravel.setEnabled(true);
            buttonEditTravel.setVisibility(View.VISIBLE);
        }else{
            buttonEditTravel.setVisibility(View.GONE);
        }


    }

    private void initialVisibility(boolean visibility){

        switchEdit.setChecked(false);
        editTextTypeCar.setEnabled(visibility);
        spinnerTimeSlot.setEnabled(visibility);
        editTextDate.setEnabled(visibility);
        editTextSeatsAvaiable.setEnabled(visibility);
        radioButtonTravelTypeHO.setEnabled(visibility);
        radioButtonTravelTypeOH.setEnabled(visibility);
        checkBoxWeeklyTravel.setEnabled(visibility);
        buttonEditTravel.setVisibility(View.GONE);



    }

    private void enableEdit(){

        switchEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchEdit.isChecked()) {
                    editTextTypeCar.setEnabled(true);
                    spinnerTimeSlot.setEnabled(true);
                    editTextDate.setEnabled(true);
                    editTextSeatsAvaiable.setEnabled(true);
                    radioButtonTravelTypeHO.setEnabled(true);
                    radioButtonTravelTypeOH.setEnabled(true);
                    checkBoxWeeklyTravel.setEnabled(true);
                    buttonEditTravel.setVisibility(View.VISIBLE);
                } else {
                    editTextTypeCar.setEnabled(false);
                    spinnerTimeSlot.setEnabled(false);
                    editTextDate.setEnabled(false);
                    editTextSeatsAvaiable.setEnabled(false);
                    radioButtonTravelTypeHO.setEnabled(false);
                    radioButtonTravelTypeOH.setEnabled(false);
                    checkBoxWeeklyTravel.setEnabled(false);
                    buttonEditTravel.setVisibility(View.GONE);

                }
            }

        });



    }

    private void verify(){

        HashMap<String,Object> filters = new HashMap<>();

        filters.put("travelId",idTravel);

        TravelRequest[] tr = ApiUtil.getTravelRequests(filters);
        boolean editable = true;
        int acceptedRequests = 0;

        for (TravelRequest travelRequest: tr) {
            if (acceptedRequests > 1) {
                // C'è un'altra richiesta accettata oltre quella del proprietario del passaggio
                editable = false;
                break;
            } else if (travelRequest.status == TravelRequest.ACCEPTED_REQUEST) {
                acceptedRequests++;
            }
        }

        if (! editable){
            Intent goToMainError = getIntent();
            setResult(RESULT_ERROR_BAD_REQUEST, goToMainError);
            finish();
        }

    }








    /*
    Inserisci un nuovo passaggio nel database cliccando sul punsante
     */
    public View.OnClickListener editTravel = new View.OnClickListener() {

        String date;
        int timeSlot;
        String typeCar;
        int seats;
        int travelType;
        int weeklyTravel;


        @Override
        public void onClick(View v) {
            boolean valid = validate();



            if (valid) {
                date = getDate();
                timeSlot = getTimeSlot();
                typeCar = getTypeCar();
                seats = getSeats();
                travelType = getTravelType();
                weeklyTravel = getWeeklyTravel();

                filters.put("newDate",date);
                filters.put("newWorkingHourId",timeSlot);
                filters.put("newCar",typeCar);
                filters.put("newSeatsNum",seats);
                filters.put("newType",travelType);
                filters.put("newWeekly",weeklyTravel);

                Intent goToMain = getIntent();
                if (ApiUtil.updateTravel(idTravel, filters)) {
                goToMain = getIntent();
                setResult(RESULT_OK, goToMain);
                finish();

                }else{
                    goToMain = getIntent();
                    setResult(RESULT_ERROR, goToMain);
                    finish();
                }
            }
        }
    };

    private void loadFromDatabase(){

        travel = ApiUtil.getTravels(filters)[0];

        editTextTypeCar.setText(travel.car);



        String dateParsed = parseDateFromDatabase(travel.date);
        editTextDate.setText(dateParsed);






        editTextSeatsAvaiable.setText(String.valueOf(travel.seats));


        Integer[] array = workingHourDatabase.values().toArray(new Integer[workingHourDatabase.size()]);

        int position= 0;

        for (int i = 0;i<workingHourDatabase.size();i++){
            if(array[i] == travel.workingHourId){
                position = i;
                break;
            }
        }


       spinnerTimeSlot.setSelection(position);



        if(travel.type == 0){
            radioButtonTravelTypeHO.setChecked(true);

        }else{
            radioButtonTravelTypeOH.setChecked(true);
        }

        if(travel.weeklyTravel == 1){
            checkBoxWeeklyTravel.setChecked(true);
        }else{
            checkBoxWeeklyTravel.setChecked(false);
        }


    }


    private String parseDateFromDatabase(String dateFromDatabase ){

        String newDate[] = dateFromDatabase.split("-");

        int year = Integer.parseInt(newDate[0]);
        int month = Integer.parseInt(newDate[1]);
        int day = Integer.parseInt(newDate[2]);

        return day + "/" + month + "/" + year;

    }

    /*
    Restituisce la data indicata dall'utente
     */
    private String getDate() {
        return editTextDate.getText().toString();
    }

    /*
    Restituisce il modello dell'auto indicata dall'utente
     */
    private String getTypeCar() {
        return editTextTypeCar.getText().toString();
    }

    /*
    Restituisce il numero di posto disponibili indicati dall'utente
     */
    private int getSeats() {

        return Integer.parseInt(editTextSeatsAvaiable.getText().toString());
    }

    /*
    Restituisce il tipo di viaggio indicato dall'utente
     */
    private int getTravelType() {
        if (radioButtonTravelTypeHO.isChecked())
                    /*
                    è stato selezionato il tipo di viaggio Casa-ufficio
                     */
            return HOME_WORK;
        else
                     /*
                    è stato selezionato il tipo di viaggio ufficio-casa
                     */
            return WORK_HOME;
    }

    /*
    Restituisce l'indicazione dell'utente riguardante la ripetitività settimanale del viaggio
    Restituisce una stringa se il viaggio è settimanale
    Altrimenti null
     */
    private int getWeeklyTravel() {
        if (checkBoxWeeklyTravel.isChecked())
                    /*
                    Il viaggio viene effettuato settimanalmente
                     */
            return WEEKLY_TRAVEL_CHECKED;
        else
                    /*
                    Il viaggio non viene effettuato settimanalmente
                     */
            return WEEKLY_TRAVEL_NOT_CHECKED;
    }

    /*
    Restituisce l'id della fascia oraria indicata dall'utente, presente nel database
     */
    private int getTimeSlot() {
        String workingHourSelected = spinnerTimeSlot.getSelectedItem().toString();


        int id = workingHourDatabase.get(workingHourSelected);


        return  id;

    }

    /*
      Gestisci il datePicker nella casella data
    */
    private void manageDatePicker() {
        editTextDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if (isFocus == false) {
                    if (editTextDate.length() > 0)
                        textInputLayoutEditDate.setError(null);
                    else
                        textInputLayoutEditDate.setError(MESSAGGE_ERROR_FIELD);
                }
                if (isFocus == true) {
                    // calender class's instance and get current date , month and year from calender
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day


                    datePickerDialog = new DatePickerDialog(it.uniba.di.sms.max2d.carpooling.EditTravelActivity.this, new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            // set day of month , month and year value in the edit text
                            editTextDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year));
                        }
                    }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            }
        });

        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                datePickerDialog = new DatePickerDialog(EditTravelActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text
                        editTextDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year));
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
                SignInActivity.closeKeyboard(EditTravelActivity.this, editTextDate.getWindowToken());
            }
        });
    }

    private void manageWorkingHourSpinner() {

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, R.layout.list_item, workingHourDatabase.keySet().toArray(new String[workingHourDatabase.size()]));

        spinnerTimeSlot.setAdapter(adapter);

    }


    /*
    Controlla che i campi obbligatori siano inseriti prima che la registrazione venga effettuata
     */
    private boolean validate() {
        Boolean correctInsert = true;

        if (getDate().length() <= 0) {
            correctInsert = false;
            textInputLayoutEditDate.setError(MESSAGGE_ERROR_FIELD);
        }

        if (getDate().length() >0){
            final Calendar c = Calendar.getInstance();



            String insertData[] = getDate().split("/");

            int insertDay = Integer.parseInt(insertData[0]);
            int insertMonth = Integer.parseInt(insertData[1]);
            int insertYear = Integer.parseInt(insertData[2]);

            if(currentYear > insertYear){
                correctInsert = false;
            }else if (currentYear == insertYear && currentMonth > insertMonth){
                correctInsert = false;
            }else if (currentYear == insertYear && currentMonth == insertMonth && currentDay > insertDay )
                correctInsert = false;

            if(!correctInsert)
                textInputLayoutEditDate.setError(getApplicationContext().getString(R.string.error_next_date));


        }

        if (getTypeCar().length() == 0) {
            correctInsert = false;
            textInputLayoutEditTypeCar.setError(MESSAGGE_ERROR_FIELD);
        }

        if (editTextSeatsAvaiable.getText().toString().length() == 0) {
            correctInsert = false;
            textInputLayoutEditSeats.setError(MESSAGGE_ERROR_FIELD);
        }


        if(editTextSeatsAvaiable.getText().toString().length() != 0){
            if(getSeats() <1){
                correctInsert = false;
                textInputLayoutEditSeats.setError(getApplicationContext().getString(R.string.message_error_seats));
            }
        }

        return correctInsert;
    }

    @Override
    public void onBackPressed() {
        Intent goBack = new Intent();
        setResult(RESULT_ANNULL, goBack);
        finish();

    }

    /**
     * Funzione per la gestione del click in
     * una delle voci del menu
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public Thread threadToolbar = new Thread(new Runnable() {
        @Override
        public void run() {
            mtoolbar.setTitle(getResources().getString(R.string.app_name));
            mtoolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
            mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
            mtoolbar.setTitleMargin(0, 0, 0, 0);
            mtoolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mtoolbar.setSubtitle(getResources().getString(R.string.edit_activity));

            setSupportActionBar(mtoolbar);

            threadDrawer.start();

            try {
                threadDrawer.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    });

    public Thread threadDrawer = new Thread(new Runnable() {
        @Override
        public void run() {
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.menulaterale);
            Menu mMenu = mNavigationView.getMenu();

            if (user.id == User.MOBILITY_DEMO || user.id == User.USER_DEMO) {
                mMenu.findItem(R.id.LogOut).setVisible(false);
                mMenu.findItem(R.id.registrazione).setVisible(true);
            }

            if(user.role==User.MOBILITY_MANAGER){
                mMenu.findItem(R.id.mobilityManage).setVisible(true);
                mMenu.findItem(R.id.mobilityList).setVisible(true);
            }

            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.Home:
                            startActivity(new Intent(getContext(), MapsActivity.class));

                            break;
                        case R.id.Travel:
                            startActivity(new Intent(getContext(), TravelActivity.class));

                            break;
                        case R.id.Classifica:
                            startActivity(new Intent(getContext(), RankingActivity.class));

                            break;
                        case R.id.mobilityManage:
                            startActivity(new Intent(getContext(), ManageCompanyActivity.class));

                            break;
                        case R.id.mobilityList:
                            startActivity(new Intent(getContext(), UserListActivity.class));

                            break;
                        case R.id.Account:
                            startActivity(new Intent(getContext(), ManageAccountActivity.class));
                            finish();
                            break;
                        case R.id.LogOut:
                            AuthManager.logout((Activity) getContext());
                            break;
                        case R.id.registrazione:
                            AuthManager.logout((Activity) getContext());
                            break;
                    }
                    return true;
                }
            });
        }
    });

    private Context getContext(){
        return this;
    }
}
